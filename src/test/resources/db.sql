SET FOREIGN_KEY_CHECKS=0;
INSERT INTO `user` (`id`, `creation_date`, `updated_date`, `img_url`, `password`, `reputation`, `role`, `username`,`promotion`,`enabled`,`etna_login`) VALUES (1, '2021-02-25 15:28:31', '2021-02-25 15:28:31', '', '$2a$10$t06i0zP5dbxKM7UdxoY74u5JtIcoTN6s8inlEpu8TV/T8MmmC3OSm',100, 'ROLE_USER','user', '4',1,'user');
INSERT INTO `user` (`id`, `creation_date`, `updated_date`, `img_url`, `password`, `reputation`, `role`, `username`,`promotion`,`enabled`,`etna_login`) VALUES (2, '2021-02-25 15:28:44', '2021-02-25 15:28:44', '', '$2a$10$tQ28.1jBbfZFkcICsfVp.OLdGZvBwfuaiCU1M3c563NuLN3oJsg6O',100, 'ROLE_ADMIN', 'admin', null,1,'admin');
INSERT INTO `user` (`id`, `creation_date`, `updated_date`, `img_url`, `password`, `reputation`, `role`, `username`,`promotion`,`enabled`,`etna_login`) VALUES (3, '2021-02-25 15:28:31', '2021-02-25 15:28:31', '', '$2a$10$t06i0zP5dbxKM7UdxoY74u5JtIcoTN6s8inlEpu8TV/T8MmmC3OSm',100, 'ROLE_USER', 'user_2', '4',1,'user_2');
INSERT INTO `promotion` (`id`, `creation_date`, `updated_date`, `name`) VALUES ('4', '2021-03-26 17:03:05', '2021-03-26 17:03:05', 'IDV-2021');
INSERT INTO `validationunit` (`id`, `creation_date`, `updated_date`, `long_name`, `name`) VALUES ('1015', '2021-05-03 21:01:37', NULL, 'Rattrapage développement en C', 'RTP-DVC4');
INSERT INTO `uv_activity` (`id`, `creation_date`, `updated_date`, `activity_name`, `uv`) VALUES ('1016', '2021-05-03 21:01:37', NULL, 'Pong', '1015');
SET FOREIGN_KEY_CHECKS=1;
