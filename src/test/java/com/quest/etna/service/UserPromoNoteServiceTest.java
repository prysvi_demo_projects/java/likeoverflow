package com.quest.etna.service;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.controller.AbstractControllerTest;
import com.quest.etna.model.Promotion;
import com.quest.etna.model.User;
import com.quest.etna.model.UserPromoNote;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("testing")
class UserPromoNoteServiceTest extends AbstractControllerTest {

    private static User refUser;
    private static Promotion refPromotion;
    private static UserPromoNote refNote;
    @Autowired
    private UserPromoNoteService service;
    @Autowired
    private PromotionService promotionService;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        refPromotion = new Promotion(4L, "IDV-2021");
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Order(1)
    @Test
    void create() {
        var ref = new UserPromoNote(refUser.getId(), refPromotion.getId(), refUser, refPromotion, 17.3);
        var res = service.create(ref);
        Assertions.assertEquals(ref, res);
        refNote = res;
    }

    @Order(2)
    @Test
    void findById() {
        Assertions.assertEquals(service.findById(refNote.getId(), false), refNote);
    }

    @Order(3)
    @Test
    void update() {
        refNote.setNote(13.31);
        Assertions.assertEquals(service.update(refNote, false), refNote);
    }

    @Order(4)
    @Test
    void delete() {
        Assertions.assertTrue(service.delete(refNote));
        Assertions.assertNull(service.findById(refNote.getId(), false));
    }
}