package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.AddressDTO;
import com.quest.etna.mapper.AddressMapper;
import com.quest.etna.model.Address;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
public class AddressControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refUser2;
    private static User refAdmin;
    private static Address refAddress;
    private static Address ref2Address;
    private String userToken;
    private String user2Token;
    private String adminToken;
    @Autowired
    private AddressMapper addressMapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/address";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refUser2 = new User(3L, "user_2", "secret", UserRole.ROLE_USER);
        user2Token = generateAuthToken(refUser2);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @Order(1)
    @DisplayName("OK: [201][TOKEN] Create own address")
    public void create_address_OK() throws Exception {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setCity("Paris");
        addressDTO.setCountry("France");
        addressDTO.setPostalCode("75000");
        addressDTO.setStreet("Rue de la Victoire");
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addressDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.city").value(addressDTO.getCity()))
                .andExpect(jsonPath("$.country").value(addressDTO.getCountry()))
                .andExpect(jsonPath("$.postalCode").value(addressDTO.getPostalCode()))
                .andExpect(jsonPath("$.street").value(addressDTO.getStreet()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refAddress = addressMapper.objFromDto(testObjectMapper.readValue(result, AddressDTO.class));

        addressDTO.setCity("Lyon");
        addressDTO.setCountry("France");
        addressDTO.setPostalCode("69000");
        addressDTO.setStreet("Rue de Saint-Jerome");

        String result2 = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + user2Token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addressDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.city").value(addressDTO.getCity()))
                .andExpect(jsonPath("$.country").value(addressDTO.getCountry()))
                .andExpect(jsonPath("$.postalCode").value(addressDTO.getPostalCode()))
                .andExpect(jsonPath("$.street").value(addressDTO.getStreet()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        ref2Address = addressMapper.objFromDto(testObjectMapper.readValue(result2, AddressDTO.class));
    }

    @Test
    @Order(2)
    @DisplayName("OK: [200][TOKEN] Update own address")
    public void update_own_address_OK() throws Exception {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setStreet("Rue Charles de Gaulle");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refAddress.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addressDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.city").value(refAddress.getCity()))
                .andExpect(jsonPath("$.country").value(refAddress.getCountry()))
                .andExpect(jsonPath("$.postalCode").value(refAddress.getPostalCode()))
                .andExpect(jsonPath("$.street").value(addressDTO.getStreet()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refAddress = addressMapper.objFromDto(testObjectMapper.readValue(result, AddressDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("FAIL: [403][TOKEN] Update stranger address")
    public void update_stranger_address_OK() throws Exception {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setStreet("Rue Charles de Gaulle");
        this.mockMvc.perform(put(rootControllerUrl + "/" + ref2Address.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addressDTO)))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(4)
    @DisplayName("OK: [200][TOKEN] User can get only own addresses")
    public void user_get_all_OK() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].owner.id", everyItem(is(refUser.getId()))))
                .andExpect(jsonPath("$[*].city").value(containsInAnyOrder(refAddress.getCity())))
                .andExpect(jsonPath("$[*].country").value(containsInAnyOrder(refAddress.getCountry())))
                .andExpect(jsonPath("$[*].postalCode").value(containsInAnyOrder(refAddress.getPostalCode())))
                .andExpect(jsonPath("$[*].street").value(containsInAnyOrder(refAddress.getStreet())))
                .andDo(print());
    }

    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] Admin can get all addresses")
    public void admin_get_all_OK() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].city", hasItem(refAddress.getCity())))
                .andExpect(jsonPath("$[*].country", hasItem(refAddress.getCountry())))
                .andExpect(jsonPath("$[*].postalCode", hasItem(refAddress.getPostalCode())))
                .andExpect(jsonPath("$[*].street", hasItem(refAddress.getStreet())))
                .andDo(print());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User Get own address")
    public void get_own_address_OK() throws Exception {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setStreet("Rue Charles de Gaulle");
        this.mockMvc.perform(get(rootControllerUrl + "/" + refAddress.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.city").value(refAddress.getCity()))
                .andExpect(jsonPath("$.country").value(refAddress.getCountry()))
                .andExpect(jsonPath("$.postalCode").value(refAddress.getPostalCode()))
                .andExpect(jsonPath("$.street").value(addressDTO.getStreet()))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl + "/users/" + refUser.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].city", hasItem(refAddress.getCity())))
                .andExpect(jsonPath("$[*].country", hasItem(refAddress.getCountry())))
                .andExpect(jsonPath("$[*].postalCode", hasItem(refAddress.getPostalCode())))
                .andExpect(jsonPath("$[*].street", hasItem(refAddress.getStreet())))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [403][TOKEN] User Get stranger address")
    public void get_stranger_address_403() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/" + ref2Address.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][TOKEN] Admin get strange address")
    public void admin_get_strange_address_OK() throws Exception {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setStreet("Rue Charles de Gaulle");
        this.mockMvc.perform(get(rootControllerUrl + "/" + refAddress.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.city").value(refAddress.getCity()))
                .andExpect(jsonPath("$.country").value(refAddress.getCountry()))
                .andExpect(jsonPath("$.postalCode").value(refAddress.getPostalCode()))
                .andExpect(jsonPath("$.street").value(addressDTO.getStreet()))
                .andDo(print());
    }

    @Test
    @Order(9)
    @DisplayName("OK: [201][TOKEN] User Delete own address")
    public void delete_own_address_201() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refAddress.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk());
    }

    @Test
    @Order(10)
    @DisplayName("FAIL: [403][TOKEN] User Delete stranger address")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + ref2Address.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(11)
    @DisplayName("OK: [200][TOKEN] Admin Delete stranger address")
    public void admin_delete_stranger_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + ref2Address.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }

}
