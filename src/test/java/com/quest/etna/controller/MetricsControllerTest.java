package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.mapper.TechnologyMapper;
import com.quest.etna.model.Technology;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class MetricsControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refAdmin;
    private static Technology refTechnology;
    private String userToken;
    private String adminToken;

    @Autowired
    private TechnologyMapper mapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/metrics";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @DisplayName("OK: [200][TOKEN] Admin get messages metrics")
    public void user_admin_get_technology_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/messages/date/weeks/4")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Fail: [403][TOKEN] Admin get messages metrics")
    public void user_get_technology_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/messages/date/weeks/4")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("OK: [200][TOKEN] Admin get users metrics")
    public void admin_get_technology_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/users/date/weeks/4")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    @DisplayName("Fail: [403][TOKEN] Admin get users metrics")
    public void user_get_technology_fail() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/users/date/weeks/4")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("OK: [200][TOKEN] Admin get questions metrics")
    public void admin_get_questions_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/questions/date/weeks/4")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    @DisplayName("Fail: [403][TOKEN] Admin get questions metrics")
    public void user_get_questions_fail() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/questions/date/weeks/4")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("OK: [200][TOKEN] Admin get active users metrics")
    public void admin_get_active_users_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/users/active")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andDo(print());
    }

    @Test
    @DisplayName("Fail: [403][TOKEN] Admin get active users metrics")
    public void user_get_active_users_fail() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/users/active")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("OK: [200][TOKEN] Admin get active popular technologies metrics")
    public void admin_get_active_popular_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/technologies/popular")
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    @DisplayName("Fail: [403][TOKEN] Admin get active popular technologies  metrics")
    public void user_get_active_popular_fail() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/technologies/popular")
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }
}