package com.quest.etna.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibatis.common.jdbc.ScriptRunner;
import com.quest.etna.QuestSpringBootApplication;
import com.quest.etna.config.EtnaConnector;
import com.quest.etna.config.InMemoryJpaConfig;
import com.quest.etna.security.jwt.JwtTokenUtil;
import com.quest.etna.config.WebSecurityConfig;
import com.quest.etna.dto.UserDTO;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {QuestSpringBootApplication.class, WebSecurityConfig.class, InMemoryJpaConfig.class, EtnaConnector.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AbstractControllerTest {
    protected static MockHttpSession authedSession;
    protected static String authToken;
    protected static String rootControllerUrl;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected ObjectMapper testObjectMapper;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    LocalContainerEntityManagerFactoryBean entityManagerFactory;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PersistenceContext
    private EntityManager entityManager;


    public void initDataBase(String backupName) {
        try {
            BufferedReader rd = new BufferedReader(new FileReader("src/test/resources/" + backupName));
            ScriptRunner sr = new ScriptRunner(entityManagerFactory.getDataSource().getConnection(), false, false);
            sr.runScript(rd);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot initialise the database : file not found");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot initialise the database : SQL issue");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot initialise the database : unexpected error");
        }
    }


    protected String getWebAuthToken(String username, String password) throws Exception {
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);
        reqUser.setPassword(password);
        String result = this.mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON).content(testObjectMapper.writeValueAsString(reqUser)))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        HashMap<String, String> mapRes = (HashMap<String, String>) objectMapper.readValue(result, HashMap.class);
        return mapRes.get("token");
    }

    protected String generateAuthToken(User user) {
        return jwtTokenUtil.generateToken(new JwtUserDetails(user));
    }
}
