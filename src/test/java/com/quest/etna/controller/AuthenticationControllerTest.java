package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.UserDTO;
import com.quest.etna.mapper.UserMapper;
import com.quest.etna.model.User;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("testing")
public class AuthenticationControllerTest extends AbstractControllerTest {

    @Autowired
    private UserMapper userMapper;
    private static User refUser;

    @BeforeAll
    public void init() {
        rootControllerUrl = "";
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }


    @Test
    @Order(1)
    @DisplayName("OK: [200] Create new user")
    void register_201() throws Exception {
        String username = "bob";
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);
        reqUser.setPassword("secret");
        reqUser.setEtnaLogin("bob");

        String result = this.mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON).content(testObjectMapper.writeValueAsString(reqUser))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username").value(username))
                .andExpect(jsonPath("$.role").value("ROLE_USER"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refUser = userMapper.objFromDto(testObjectMapper.readValue(result, UserDTO.class));
    }

    @Test
    @Order(2)
    @DisplayName("FAIL: [409] Create new user , user already exist")
    void register_409() throws Exception {
        String username = "bob";
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);
        reqUser.setPassword("secret");
        this.mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON).content(testObjectMapper.writeValueAsString(reqUser)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    @Order(3)
    @DisplayName("FAIL: [500] Create new user , missed values")
    void register_500() throws Exception {
        String username = "bob";
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);

        this.mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testObjectMapper.writeValueAsString(reqUser)))
                .andDo(print())
                .andExpect(status().isInternalServerError());
    }

    @Test
    @Order(5)
    @DisplayName("FAIL: [500] wrong credentials")
    void auth_FAIL() throws Exception {
        String username = "bob";
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);
        reqUser.setPassword("wrong");
        this.mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON).content(testObjectMapper.writeValueAsString(reqUser)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isInternalServerError());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200] authenticate success")
    void auth_OK() throws Exception {
        String username = "bob";
        UserDTO reqUser = new UserDTO();
        reqUser.setUsername(username);
        reqUser.setPassword("secret");
        String result = this.mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON).content(testObjectMapper.writeValueAsString(reqUser)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        HashMap<String, String> mapRes = (HashMap<String, String>) objectMapper.readValue(result, HashMap.class);
        authToken = mapRes.get("token");
    }

    @Test
    @Order(7)
    @DisplayName("OK: [200] me matching to expected user")
    void me_OK() throws Exception {
        this.mockMvc.perform(get("/me")
                .header("Authorization", "Bearer " + authToken))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.username").value(refUser.getUsername()))
                .andExpect(jsonPath("$.enabled").value("true"))
                .andExpect(jsonPath("$.authorities").isArray())
                .andExpect(jsonPath("$.authorities", hasSize(1)))
                .andExpect(jsonPath("$.accountNonLocked").value("true"))
                .andExpect(jsonPath("$.accountNonExpired").value("true"))
                .andExpect(jsonPath("$.credentialsNonExpired").value("true"))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }
}
