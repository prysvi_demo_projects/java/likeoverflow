package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.UserDTO;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
public class UserControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refAdmin;
    private String userToken;
    private String adminToken;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/user";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }


    @Test
    @Order(1)
    @DisplayName("OK: [200][Token] get user by id")
    void getUser_200() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/" + refUser.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(refUser.getUsername()))
                .andExpect(jsonPath("$.role").value(refUser.getRole().name()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        this.mockMvc.perform(get(rootControllerUrl + "/" + refAdmin.getId())
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value(refAdmin.getUsername()))
                .andExpect(jsonPath("$.role").value(refAdmin.getRole().name()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(2)
    @DisplayName("FAIL: [401][No Token]Unauthorized")
    void getUser_401() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/" + refUser.getId()))
                .andExpect(status().isUnauthorized());

        this.mockMvc.perform(get(rootControllerUrl + "/" + refAdmin.getId()))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][Token] Get all users")
    void getAllUsers_200() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();

        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(4)
    @DisplayName("OK: [401][No Token] Get all users")
    void getAllUsers_401() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(5)
    @DisplayName("OK: [403][Token] User can't update another")
    void another_update_FAIL() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setUsername("toto");
        this.mockMvc.perform(put(rootControllerUrl + "/" + 3)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUser)))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][Token] User can update himself")
    void self_update_OK() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setImgUrl("toto");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refUser.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.imgUrl").value("toto"))
                .andExpect(jsonPath("$.role").value(refUser.getRole().name()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [401][Token] User can't update own ROLE")
    void self_role_update_FAIL() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setRole(UserRole.ROLE_ADMIN);
        this.mockMvc.perform(put(rootControllerUrl + "/" + refUser.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUser)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][Token] Admin can update another user")
    void admin_another_update_OK() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setImgUrl("toto");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refUser.getId())
                .header("Authorization", "Bearer " + adminToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.imgUrl").value("toto"))
                .andExpect(jsonPath("$.role").value(refUser.getRole().name()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(9)
    @DisplayName("OK: [200][Token] User can't delete himself")
    void self_delete_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refUser.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk());
    }

    @Test
    @Order(10)
    @DisplayName("FAIL: [403][Token] User can't delete another user")
    void user_delete_another_FAIL() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setUsername("toto");
        this.mockMvc.perform(delete(rootControllerUrl + "/" + 3)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(11)
    @DisplayName("OK: [200][Token] Admin can delete another user")
    void admin_delete_another_OK() throws Exception {
        UserDTO updateUser = new UserDTO();
        updateUser.setUsername("toto");
        this.mockMvc.perform(delete(rootControllerUrl + "/" + 3)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(status().isOk());
    }

}
