package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.CommentDTO;
import com.quest.etna.mapper.CommentMapper;
import com.quest.etna.model.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class CommentControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refUser2;
    private static User refAdmin;
    private static Message refMessage;
    private static Comment refComment;
    private String userToken;
    private String userToken2;
    private String adminToken;

    @Autowired
    private CommentMapper commentMapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("message.sql");
        rootControllerUrl = "/comments";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
        refUser2 = new User(3L, "user_2", "secret", UserRole.ROLE_USER);
        userToken2 = generateAuthToken(refUser2);
        refMessage = new Message();
        refMessage.setId(5L);
    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

//    @Test
//    @Order(1)
//    @DisplayName("FAIL: [403][TOKEN] User cant create comment")
//    public void user_create_comment_ok() throws Exception {
//        CommentDTO commentDTO = new CommentDTO();
//        commentDTO.setContent("randImgUrl1");
//        String result = this.mockMvc.perform(post(rootControllerUrl)
//                .header("Authorization", "Bearer " + userToken)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(commentDTO)))
//                .andExpect(status().isForbidden())
//                .andDo(print())
//                .andReturn().getResponse().getContentAsString();
//    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] User can create comment")
    public void create_comment_ok() throws Exception {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent("some random content");
        String result = this.mockMvc.perform(post(rootControllerUrl+"/messages/"+refMessage.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(commentDTO)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.content").value(commentDTO.getContent()))
                .andReturn().getResponse().getContentAsString();
        refComment = commentMapper.objFromDto(testObjectMapper.readValue(result, CommentDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] User can update comment")
    public void admin_patch_comment_ok() throws Exception {
        CommentDTO commentDTO = commentMapper.objToDto(refComment);
        commentDTO.setContent("Changed Content");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refComment.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(commentDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").value(commentDTO.getContent()))
                .andReturn().getResponse().getContentAsString();
        refComment = commentMapper.objFromDto(testObjectMapper.readValue(result, CommentDTO.class));
    }

    @Test
    @Order(4)
    @DisplayName("FAIL: [403][TOKEN] User can't update strange comment")
    public void user_patch_comment_fail() throws Exception {
        CommentDTO commentDTO = commentMapper.objToDto(refComment);
        commentDTO.setContent("asdasdasd");
        this.mockMvc.perform(put(rootControllerUrl + "/" + refComment.getId())
                .header("Authorization", "Bearer " + userToken2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(commentDTO)))
                .andExpect(status().isForbidden())
                .andDo(print());
    }


    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User can get all message comments")
    public void user_admin_get_comment_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl+"/messages/"+refMessage.getId())
                .header("Authorization", "Bearer " + userToken))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));

    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User can search comment by content")
    public void user_admin_find_by_name_comment_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/_search")
                .param("keyword", refComment.getContent())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(7)
    @DisplayName("FAIL: [403][TOKEN] User Delete strange comment")
    public void delete_stranger_address_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refComment.getId())
                .header("Authorization", "Bearer " + userToken2))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("OK: [200][TOKEN] User Delete own comment")
    public void admin_delete_stranger_address_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refComment.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isOk());
    }
}