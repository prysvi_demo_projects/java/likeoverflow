package com.quest.etna.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.quest.etna.dto.MessageDTO;
import com.quest.etna.dto.QuestionDTO;
import com.quest.etna.dto.ValidationUnitActivityDTO;
import com.quest.etna.mapper.PromotionMapper;
import com.quest.etna.mapper.QuestionMapper;
import com.quest.etna.model.Promotion;
import com.quest.etna.model.Question;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("testing")
class QuestionControllerTest extends AbstractControllerTest {
    private static User refUser;
    private static User refUser2;
    private static User refAdmin;
    private static Promotion refPromotion;
    private static Question refQuestion;
    private String userToken;
    private String userToken2;
    private String adminToken;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private PromotionMapper promotionMapper;

    @BeforeAll
    public void init() {
        testObjectMapper.disable(MapperFeature.USE_ANNOTATIONS);
        initDataBase("clean.sql");
        initDataBase("db.sql");
        rootControllerUrl = "/questions";
        refUser = new User(1L, "user", "secret", UserRole.ROLE_USER);
        userToken = generateAuthToken(refUser);
        refAdmin = new User(2L, "admin", "secret", UserRole.ROLE_ADMIN);
        adminToken = generateAuthToken(refAdmin);
        refPromotion = new Promotion(4L, "IDV-2021");
        refUser2 = new User(3L, "user_2", "secret", UserRole.ROLE_USER);
        userToken2 = generateAuthToken(refUser2);

    }

    @AfterAll
    public void destroy() {
        initDataBase("clean.sql");
    }

    @Test
    @Order(1)
    @DisplayName("FAIL: [403][TOKEN] User cant create question without uv / message")
    public void user_create_question_ok() throws Exception {
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setTitle("Problem Java");
        questionDTO.setQuestionMessage(null);
        this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionDTO)))
                .andExpect(status().isNotAcceptable())
                .andExpect(status().reason(containsString("UV must not be null")))
                .andDo(print());

        questionDTO.setPromotion(promotionMapper.objToDto(refPromotion));
        questionDTO.setUvActivity(new ValidationUnitActivityDTO(1016L, null));

        this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionDTO)))
                .andExpect(status().isNotAcceptable())
                .andExpect(status().reason(containsString("Message must not be null")))
                .andDo(print());
    }

    @Test
    @Order(2)
    @DisplayName("OK: [201][TOKEN] User can create question")
    public void admin_create_question_ok() throws Exception {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setContent("How to compile with maven?");
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setTitle("Problem Java");
        questionDTO.setUvActivity(new ValidationUnitActivityDTO(1016L, null));
        questionDTO.setQuestionMessage(messageDTO);
        questionDTO.setPromotion(promotionMapper.objToDto(refPromotion));
        String result = this.mockMvc.perform(post(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value(questionDTO.getTitle()))
                .andExpect(jsonPath("$.questionMessage.content").value(messageDTO.getContent()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refQuestion = questionMapper.objFromDto(testObjectMapper.readValue(result, QuestionDTO.class));
    }

    @Test
    @Order(3)
    @DisplayName("OK: [200][TOKEN] User can update question")
    public void user_update_question_ok() throws Exception {
        refQuestion.getQuestionMessage().setContent("How to compile with maven?");
        String result = this.mockMvc.perform(put(rootControllerUrl + "/" + refQuestion.getId())
                .header("Authorization", "Bearer " + userToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionMapper.objToDto(refQuestion))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value(refQuestion.getTitle()))
                .andDo(print())
                .andReturn().getResponse().getContentAsString();
        refQuestion = questionMapper.objFromDto(testObjectMapper.readValue(result, QuestionDTO.class));
    }

//    @Test
//    @Order(4)
//    @DisplayName("FAIL: [403][TOKEN] User can't update question")
//    public void user_patch_question_fail() throws Exception {
//        QuestionDTO questionDTO = new QuestionDTO();
//        questionDTO.setId(refQuestion.getId());
//        questionDTO.setImgUrl("randImgUrlChanged");
//        questionDTO.setName("SergantChanged");
//        this.mockMvc.perform(put(rootControllerUrl + "/" + refQuestion.getId())
//                .header("Authorization", "Bearer " + userToken)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(questionDTO)))
//                .andExpect(status().isForbidden())
//                .andDo(print());
//    }
//

    @Test
    @Order(5)
    @DisplayName("OK: [200][TOKEN] User / Admin can get all questions")
    public void user_admin_get_question_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl)
                .header("Authorization", "Bearer " + adminToken))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andDo(print());
    }

    @Test
    @Order(6)
    @DisplayName("OK: [200][TOKEN] User can search question by criteria")
    public void user_admin_find_by_name_question_ok() throws Exception {
        this.mockMvc.perform(get(rootControllerUrl + "/_search")
                .param("title", refQuestion.getTitle())
                .header("Authorization", "Bearer " + userToken))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("uvId",refQuestion.getUvActivity().getId().toString())
                .header("Authorization", "Bearer " + userToken))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andDo(print());

        this.mockMvc.perform(get(rootControllerUrl+"/_search")
                .param("title",refQuestion.getTitle())
                .param("uvId",refQuestion.getUvActivity().getId().toString())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));

    }

    @Test
    @Order(7)
    @DisplayName("OK: [403][TOKEN] User can't delete stranger question")
    public void delete_stranger_question_403() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refQuestion.getId())
                .header("Authorization", "Bearer " + userToken2))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    @DisplayName("FAIL: [403][TOKEN] User delete own question with messages")
    public void delete_own_question_OK() throws Exception {
        this.mockMvc.perform(delete(rootControllerUrl + "/" + refQuestion.getId())
                .header("Authorization", "Bearer " + userToken))
                .andExpect(status().isForbidden());
    }
}