package com.quest.etna.repository;

import com.quest.etna.QuestSpringBootApplication;
import com.quest.etna.config.InMemoryJpaConfig;
import com.quest.etna.config.WebSecurityConfig;
import com.quest.etna.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ActiveProfiles("testing")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {QuestSpringBootApplication.class, WebSecurityConfig.class, InMemoryJpaConfig.class})
@Transactional
public class InMemoryDBTest {

    @Resource
    private UserRepository userRepository;

    @Test
    public void create_and_compare_user_in_memory() {
        User testUser = new User();
        testUser.setUsername("testy");
        testUser.setEtnaLogin("testy");
        testUser.setPassword("1234523532");
        testUser.setCreationDate(new Date());
        testUser.setUpdatedDate(new Date());
        userRepository.save(testUser);

        User foundUser = userRepository.findByUsername(testUser.getUsername());
        assertEquals(testUser, foundUser);
    }
}