package com.quest.etna.exception;

import java.util.Date;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.spel.SpelEvaluationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.quest.etna.model.ErrorMessage;


@RestControllerAdvice
public class RestErrorsHandlerAdvice extends ResponseEntityExceptionHandler {
	public static Logger _log = LoggerFactory.getLogger(RestErrorsHandlerAdvice.class);

	// Method 2
	@ExceptionHandler(value = { CustomAuthenticationException.class })
	protected ResponseEntity<ErrorMessage> handleConflict(CustomAuthenticationException ex, WebRequest request) {
		ErrorMessage errorMsg = new ErrorMessage();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		errorMsg.setPath(((ServletWebRequest) request).getRequest().getRequestURI().toString());
		errorMsg.setMessage(ex.getMessage()); // JSON avec un message clair
		errorMsg.setTimestamp(new Date());
		errorMsg.setStatus(status.value());
		errorMsg.setError(status.getReasonPhrase());
		return new ResponseEntity<ErrorMessage>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = {MultipartException.class})
	protected ResponseEntity<ErrorMessage> handleConflict(MultipartException ex, WebRequest request) {
		ErrorMessage errorMsg = new ErrorMessage();
		HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
		errorMsg.setPath(((ServletWebRequest) request).getRequest().getRequestURI().toString());
		errorMsg.setMessage(ex.getMessage()); // JSON avec un message clair
		errorMsg.setTimestamp(new Date());
		errorMsg.setStatus(status.value());
		errorMsg.setError(status.getReasonPhrase());
		return new ResponseEntity<ErrorMessage>(errorMsg, status);
	}
	@ExceptionHandler(value = { HibernateException.class })
	protected ResponseEntity<ErrorMessage> handleConflict(HibernateException ex, WebRequest request) {
		ErrorMessage errorMsg = new ErrorMessage();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		errorMsg.setPath(((ServletWebRequest) request).getRequest().getRequestURI().toString());
		errorMsg.setMessage(ex.getMessage()); // JSON avec un message clair
		errorMsg.setTimestamp(new Date());
		errorMsg.setStatus(status.value());
		errorMsg.setError(status.getReasonPhrase());
		return new ResponseEntity<ErrorMessage>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { SpelEvaluationException.class })
	protected ResponseEntity<ErrorMessage> handleConflict(SpelEvaluationException ex, WebRequest request) {
		ErrorMessage errorMsg = new ErrorMessage();
		HttpStatus status = HttpStatus.FORBIDDEN;
		errorMsg.setPath(((ServletWebRequest) request).getRequest().getRequestURI().toString());
		_log.warn(ex.getClass()+" : "+ex.getMessage());
		errorMsg.setMessage("Only admin, can modify deleted user's entities");
		errorMsg.setTimestamp(new Date());
		errorMsg.setStatus(status.value());
		errorMsg.setError(status.getReasonPhrase());
		return new ResponseEntity<ErrorMessage>(errorMsg, status);
	}

}