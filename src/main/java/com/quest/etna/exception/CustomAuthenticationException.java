package com.quest.etna.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class CustomAuthenticationException extends RuntimeException {
	private static final long serialVersionUID = 195154836212954618L;

	public CustomAuthenticationException() {
		super();
	}

	public CustomAuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomAuthenticationException(String message) {
		super(message);
	}

	public CustomAuthenticationException(Throwable cause) {
		super(cause);
	}
}
