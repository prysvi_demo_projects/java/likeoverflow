package com.quest.etna.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class EtnaApiException extends RuntimeException {
	private static final long serialVersionUID = 195154836212954618L;

	public EtnaApiException() {
		super("External API not available, try later");
	}

	public EtnaApiException(String message, Throwable cause) {
		super(message, cause);
	}

	public EtnaApiException(String message) {
		super(message);
	}

	public EtnaApiException(Throwable cause) {
		super(cause);
	}
}
