package com.quest.etna.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.quest.etna.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String username;

    private Integer reputation;

    private UserRole role;

    private GradeDTO grade;

    private PromotionDTO promotion;

    private List<TechnologyDTO> technologies;

    private String imgUrl;

    private String etnaLogin;

    private boolean enabled;

}