package com.quest.etna.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageDTO {
    protected Date creationDate;
    private Long id;
    private UserDTO user;
    private String content;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long ratingScore;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<UserMessageRatingDTO> userMessageRatings;

    private List<CommentDTO> comments;

    private List<TimelineDTO> timelines;

    private Date updatedDate;
}