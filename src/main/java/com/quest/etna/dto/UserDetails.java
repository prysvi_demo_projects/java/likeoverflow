package com.quest.etna.dto;

import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetails {
    public String username;
    public UserRole role;

    public UserDetails(User user) {
        this.username = user.getUsername();
        this.role = user.getRole();
    }
}
