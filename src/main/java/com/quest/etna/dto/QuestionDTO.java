package com.quest.etna.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.quest.etna.model.ValidationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {
    protected Date creationDate;
    private Long id;
    private UserDTO user;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer views;
    private String title;
    private PromotionDTO promotion;
    private ValidationStatus status;
    @NotNull
    private MessageDTO questionMessage;
    private Date updatedDate;

    private ValidationUnitActivityDTO uvActivity;
}