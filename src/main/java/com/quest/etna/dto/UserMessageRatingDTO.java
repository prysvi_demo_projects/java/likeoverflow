package com.quest.etna.dto;

import com.quest.etna.model.Reaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMessageRatingDTO {
    private UserDTO user;
    private Reaction reaction;
}
