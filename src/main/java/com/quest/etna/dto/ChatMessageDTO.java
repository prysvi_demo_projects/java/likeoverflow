package com.quest.etna.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDTO {
    protected Date creationDate;
    private Long id;
    private UserDTO user;
    private String content;

    public ChatMessageDTO(Long id, UserDTO user, String content) {
        this.id = id;
        this.user = user;
        this.content = content;
    }
}