package com.quest.etna.dto;

import com.quest.etna.model.Action;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimelineDTO {
    protected Date creationDate;
    private Long id;
    private Action action;
    private UserDTO user;
}