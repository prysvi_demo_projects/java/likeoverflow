package com.quest.etna.aspect;

import com.quest.etna.dto.MessageDTO;
import com.quest.etna.dto.QuestionDTO;
import com.quest.etna.mapper.MessageMapper;
import com.quest.etna.model.Action;
import com.quest.etna.model.Comment;
import com.quest.etna.model.Message;
import com.quest.etna.model.Timeline;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.TimelineService;
import lombok.Data;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Aspect
@Component
@Data
public class TimelineAspect {

    private final TimelineService timelineService;

    private final SecurityProvider securityProvider;

    private final MessageMapper messageMapper;

    private final MessageService messageService;

    @AfterReturning(pointcut = "execution(* com.quest.etna.controller.MessageController.*(..))", returning = "result")
    private void messageServiceAspect(JoinPoint jp, Object result) {
        Action action;
        Message message = null;
        switch (jp.getSignature().getName()) {
            case "create":
                action = Action.CREATED;
                message = messageMapper.objFromDto((MessageDTO) result);
                break;
            case "update":
                action = Action.UPDATED;
                message = messageMapper.objFromDto((MessageDTO) result);
                break;
            case "rateMessage":
                Long msgId = (Long) jp.getArgs()[1];
                action = Action.REACTED;
                message = messageService.findById(msgId, false);
                break;
            default:
                action = null;
        }
        if (null != action && message != null) {
            timelineService.create(new Timeline(null, message, action, securityProvider.getAuthedUserModel()));
        }
    }

    @AfterReturning(pointcut = "execution(* com.quest.etna.controller.QuestionController.create(..))", returning = "result")
    private void questionControllerAspect(JoinPoint jp, QuestionDTO result) {
        Action action;
        Message message = null;
        if ("create".equals(jp.getSignature().getName())) {
            action = Action.CREATED;
            message = messageMapper.objFromDto(result.getQuestionMessage());
        } else {
            action = null;
        }
        if (null != action && message != null) {
            timelineService.create(new Timeline(null, message, action, securityProvider.getAuthedUserModel()));
        }
    }

    @AfterReturning(pointcut = "execution(* com.quest.etna.service.CommentService.create(..))", returning = "commentResult")
    private void messageAspectViaComment(JoinPoint jp, Comment commentResult) {
        Action action;
        if ("create".equals(jp.getSignature().getName())) {
            action = Action.COMMENTED;
        } else {
            action = null;
        }
        if (null != action) {
            timelineService.create(new Timeline(null, commentResult.getMessage(), action,
                    securityProvider.getAuthedUserModel()));
        }
    }
}
