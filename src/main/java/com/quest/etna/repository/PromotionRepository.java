package com.quest.etna.repository;

import com.quest.etna.model.Promotion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface PromotionRepository extends PagingAndSortingRepository<Promotion, Long> {

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("SELECT t FROM Promotion t WHERE t.name LIKE CONCAT('%',:name,'%') order by t.name")
    List<Promotion> findAllByName(String name, Pageable pageable);
}
