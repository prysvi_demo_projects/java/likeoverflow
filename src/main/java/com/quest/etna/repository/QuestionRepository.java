package com.quest.etna.repository;

import com.quest.etna.model.Question;
import com.quest.etna.model.ValidationUnitActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.Date;
import java.util.List;

@Repository
public interface QuestionRepository extends PagingAndSortingRepository<Question, Long> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("SELECT t FROM Question t WHERE t.title LIKE CONCAT('%',:title,'%') order by t.title")
    List<Question> findAllByTitle(String title, Pageable pageable);

    Page<Question> findAllByUvActivity(ValidationUnitActivity uvActivity, Pageable pageable);

    Page<Question> findAllByTitleContainsAndUvActivityContains(String title, ValidationUnitActivity uvActivity, Pageable pageable);

    List<Question> findAllByCreationDateBetween(Date start, Date end);

}
