package com.quest.etna.repository;

import com.quest.etna.model.UserPromoNote;
import com.quest.etna.model.UserPromoNoteId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserPromoNoteRepository extends PagingAndSortingRepository<UserPromoNote, UserPromoNoteId> {
    @Override
    Optional<UserPromoNote> findById(UserPromoNoteId userPromoNoteId);

    Page<UserPromoNote> findAllByPromoId(Long promoId, Pageable pageable);
}
