package com.quest.etna.repository;

import com.quest.etna.model.ValidationUnitActivity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface ValidationUnitActivityRepository extends PagingAndSortingRepository<ValidationUnitActivity, Long> {

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("SELECT t FROM uv_activity t WHERE t.activityName LIKE CONCAT('%',:name,'%') order by t.activityName")
    List<ValidationUnitActivity> findAllByName(String name, Pageable pageable);

    List<ValidationUnitActivity> findAllByValidationUnitIdAndActivityName(Long unitId, String activityName);
}
