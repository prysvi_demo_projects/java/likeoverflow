package com.quest.etna.repository;

import com.quest.etna.model.Address;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends PagingAndSortingRepository<Address, Long> {

    @Query("SELECT a FROM Address a WHERE a.user.id = :userId")
    Page<Address> findAllByUserId(long userId, Pageable pageable);
}
