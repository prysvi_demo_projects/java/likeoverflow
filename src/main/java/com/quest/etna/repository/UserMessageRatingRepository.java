package com.quest.etna.repository;

import com.quest.etna.model.UserMessageRating;
import com.quest.etna.model.UserMessageRatingId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserMessageRatingRepository extends PagingAndSortingRepository<UserMessageRating, UserMessageRatingId> {
    @Override
    Optional<UserMessageRating> findById(UserMessageRatingId userMessageRatingId);
}
