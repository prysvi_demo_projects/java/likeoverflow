package com.quest.etna.repository;

import com.quest.etna.model.ChatMessage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatMessageRepository extends PagingAndSortingRepository<ChatMessage, Long> {
    @Override
    @Query("SELECT u FROM ChatMessage u")
    List<ChatMessage> findAll();
}
