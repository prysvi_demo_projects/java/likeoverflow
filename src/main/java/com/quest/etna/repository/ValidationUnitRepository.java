package com.quest.etna.repository;

import com.quest.etna.model.ValidationUnit;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

@Repository
public interface ValidationUnitRepository extends PagingAndSortingRepository<ValidationUnit, Long> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("SELECT t FROM ValidationUnit t WHERE t.longName LIKE CONCAT('%',:longName,'%') order by t.longName")
    List<ValidationUnit> findAllByLongNameContains(String longName, Pageable pageable);

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Query("SELECT t FROM ValidationUnit t WHERE t.name LIKE CONCAT('%',:name,'%') order by t.name")
    List<ValidationUnit> findAllByNameContains(String name, Pageable pageable);

    Optional<ValidationUnit> findByName(String name);
}
