package com.quest.etna.repository;

import com.quest.etna.model.Grade;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface GradeRepository extends PagingAndSortingRepository<Grade, Long> {

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("SELECT t FROM Grade t WHERE t.name LIKE CONCAT('%',:name,'%') order by t.name")
    List<Grade> findAllByName(String name, Pageable pageable);
}
