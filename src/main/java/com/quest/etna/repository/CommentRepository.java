package com.quest.etna.repository;

import com.quest.etna.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.QueryHint;
import java.util.List;

@Repository
public interface CommentRepository extends PagingAndSortingRepository<Comment, Long> {

    @QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
    @Query("SELECT t FROM Comment t WHERE t.content LIKE CONCAT('%',:content,'%') order by t.content")
    List<Comment> findAllByContentContainsText(String content, Pageable pageable);

    Page<Comment> findAllByMessage_Id(Long messageId, Pageable pageable);
}
