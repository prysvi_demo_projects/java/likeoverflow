package com.quest.etna.repository;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageRepository {
    String store(MultipartFile file);

    void deleteAll();

    Stream<Path> loadAll();

    boolean delete(String filename);

    String storeUserMultipartFile(MultipartFile file, String userId);
}
