package com.quest.etna.repository;

import com.quest.etna.model.Timeline;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimelineRepository extends PagingAndSortingRepository<Timeline, Long> {
//
//    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
//    @Query("SELECT t FROM Promotion t WHERE t.name LIKE CONCAT('%',:name,'%') order by t.name")
//    List<Timeline> findAllByName(String name, Pageable pageable);

    Page<Timeline> findAllByMessageId(Long messageId, Pageable pageable);

    Page<Timeline> findAllByUserId(Long messageId, Pageable pageable);
}
