package com.quest.etna.controller;

import com.quest.etna.dto.ChatMessageDTO;
import com.quest.etna.mapper.ChatMessageMapper;
import com.quest.etna.model.ChatMessage;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.ChatMessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class ChatMessageController {

    private final ChatMessageService chatMessageService;
    private final SimpMessagingTemplate messagingTemplate;
    private final SecurityProvider securityProvider;
    private final ChatMessageMapper chatMessageMapper;
    @Value("${app.sockets.room_name:global}")
    private String roomName;

    @MessageMapping("/chat/messages")
    public void processRoomMessage(@Payload ChatMessage chatMessage) {
        chatMessage.setId(null);
        chatMessage.setUser(securityProvider.getAuthedUserModel());
        messagingTemplate.convertAndSendToUser(roomName, "/queue/messages",
                chatMessageMapper.objToDto(chatMessageService.create(chatMessage)));

    }

    @GetMapping("/chat/messages")
    @ResponseBody
    public List<ChatMessageDTO> getAllMessages(
            @RequestParam(value = "sortDir", required = false, defaultValue = "DESC") Sort.Direction sortDir,
            @RequestParam(value = "sortField", required = false, defaultValue = "creationDate") String sortField,
            @RequestParam(value = "perPage", required = false, defaultValue = "10") int perPage,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return chatMessageMapper.objListToDtoList(chatMessageService.findAll(PageRequest.of(page, perPage,
                Sort.by(sortDir, sortField))));
    }


    @PreAuthorize("hasRole('ROLE_ADMIN') or @chatMessageService.findById(#id, true).getUser().getId()==authentication.id")
    @DeleteMapping("/chat/messages/{id}")
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Long id) {
        chatMessageService.delete(id);
        messagingTemplate.convertAndSendToUser(roomName, "/queue/messages", new ChatMessageDTO(id, null, null));
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", chatMessageService.findById(id, false) == null);
            }
        };
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or @chatMessageService.findById(#id, true).getUser().getId()==authentication.id")
    @PutMapping("/chat/messages/{id}")
    @ResponseBody
    public ChatMessageDTO update(@RequestBody ChatMessageDTO chatMessageDTO, @PathVariable Long id) {
        chatMessageDTO.setId(id);
        ChatMessage chatMessageResult = chatMessageService.update(chatMessageMapper.objFromDto(chatMessageDTO));
        ChatMessageDTO resultDto = chatMessageMapper.objToDto(chatMessageResult);
        messagingTemplate.convertAndSendToUser(roomName, "/queue/messages", resultDto);
        return resultDto;
    }

}
