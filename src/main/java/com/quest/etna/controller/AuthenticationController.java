package com.quest.etna.controller;

import com.quest.etna.dto.UserDTO;
import com.quest.etna.dto.UserDetails;
import com.quest.etna.exception.CustomAuthenticationException;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.security.jwt.JwtTokenUtil;
import com.quest.etna.security.jwt.JwtUserDetailsService;
import com.quest.etna.service.ReputationService;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;


@RequiredArgsConstructor
@RestController
public class AuthenticationController {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtUserDetailsService jwtUserDetailsService;
    private final ReputationService reputationService;

    @PostMapping("/register")
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDetails register(@RequestBody UserDTO reqUser) {

        if (reqUser.getUsername() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "username missed");
        }

        if (reqUser.getPassword() == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "password missed");
        }


        if (userService.findByUserName(reqUser.getUsername(), false) != null) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "user already exists");
        }

        try {
            User user = new User();
            user.setUsername(reqUser.getUsername());
            user.setEtnaLogin(reqUser.getEtnaLogin());
            user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
            user.setEnabled(true);
            reputationService.refreshUserReputation(user, 0);
            return new UserDetails(userService.create(user));
        } catch (DataIntegrityViolationException ex) {
            Throwable cause = ex.getCause();
            if (cause instanceof ConstraintViolationException) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
            } else {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PostMapping("/authenticate")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<?> authenticate(@RequestBody User reqUser) {
        try {
            User user = userService.findByUserName(reqUser.getUsername(), false);
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(reqUser.getUsername(), reqUser.getPassword()));
            String token = jwtTokenUtil.generateToken(new JwtUserDetails(user));
            Map<Object, Object> response = new HashMap<>();
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            throw new CustomAuthenticationException(e.getMessage());
        }
    }

    @GetMapping("/me")
    @ResponseStatus(code = HttpStatus.OK)
    public org.springframework.security.core.userdetails.UserDetails me() {
        try {
            return jwtUserDetailsService.loadUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        } catch (RuntimeException e) {
            throw new CustomAuthenticationException(e.getMessage());
        }
    }


    // Method 1
//	@ExceptionHandler(value = {CustomAuthenticationException.class })
//	protected ResponseEntity<ErrorMessage> handleConflict(CustomAuthenticationException ex, WebRequest request) {
//		ErrorMessage errorMsg = new ErrorMessage();
//		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
//		errorMsg.setPath(((ServletWebRequest)request).getRequest().getRequestURI().toString());
//		errorMsg.setMessage(ex.getMessage()); // JSON avec un message clair
//		errorMsg.setTimestamp(new Date());
//		errorMsg.setStatus(status.value());
//		errorMsg.setError(status.getReasonPhrase());
//		return new ResponseEntity<ErrorMessage>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
//	}
}
