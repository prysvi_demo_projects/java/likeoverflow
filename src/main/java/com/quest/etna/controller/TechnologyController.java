package com.quest.etna.controller;

import com.quest.etna.dto.TechnologyDTO;
import com.quest.etna.mapper.TechnologyMapper;
import com.quest.etna.service.TechnologyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("technologies")
public class TechnologyController {

    private final TechnologyService technologyService;

    private final TechnologyMapper technologyMapper;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public TechnologyDTO create(@RequestBody TechnologyDTO dto) {
        return technologyMapper.objToDto(technologyService.create(technologyMapper.objFromDto(dto)));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public TechnologyDTO update(@RequestBody TechnologyDTO dto, @PathVariable Long id) {
        dto.setId(id);
        return technologyMapper.objToDto(technologyService.update(technologyMapper.objFromDto(dto)));
    }

    @GetMapping("{id}")
    public TechnologyDTO get(@PathVariable Long id) {
        return technologyMapper.objToDto(technologyService.findById(id, true));
    }

    @GetMapping("")
    public List<TechnologyDTO> findAll(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                       @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return technologyMapper.objListToDtoList(technologyService.findAll(PageRequest.of(page, perPage)));
    }

    @GetMapping("/_search")
    public List<TechnologyDTO> findAllByName(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                             @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                             @RequestParam(value = "keyword", required = true) String keyword) {
        return technologyMapper.objListToDtoList(technologyService.findAllByName(keyword, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", technologyService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }
}
