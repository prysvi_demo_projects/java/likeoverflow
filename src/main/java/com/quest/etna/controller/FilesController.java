package com.quest.etna.controller;

import com.quest.etna.service.FileSystemStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping("/files")
public class FilesController {

    private final List<String> allowedTypes = Arrays.asList("image/png", "image/jpeg", "image/gif");

    private final FileSystemStorageService fileStorageService;

    @PostMapping("")
    public String store(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        if (!allowedTypes.contains(file.getContentType())) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Not allowed filetype!!! Allowed types ="
                    + allowedTypes.stream().map(t -> t.substring(t.lastIndexOf("/")))
                    .collect(Collectors.toList()));
        }

        return "${api}" + fileStorageService.store(file);
    }

    @DeleteMapping("/{folderId}/{filename}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or #folderId==authentication.etnaLogin")
    public ResponseEntity<?> delete(@PathVariable Long folderId, @PathVariable String filename) {

        Map<String, Object> response = new HashMap<>();
        response.put("file", filename);
        response.put("removed", fileStorageService.delete(folderId + "/" + filename));
        return ResponseEntity.ok(response);
    }
}
