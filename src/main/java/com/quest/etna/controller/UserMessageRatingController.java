package com.quest.etna.controller;

import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("rating")
public class UserMessageRatingController {

    private final MessageService messageService;

    private final UserService userService;

    private final SecurityProvider securityProvider;


}
