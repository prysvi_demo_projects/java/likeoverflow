package com.quest.etna.controller;

import com.quest.etna.dto.CommentDTO;
import com.quest.etna.mapper.CommentMapper;
import com.quest.etna.mapper.MessageMapper;
import com.quest.etna.mapper.UserMapper;
import com.quest.etna.mapper.UserMessageRatingMapper;
import com.quest.etna.model.Comment;
import com.quest.etna.model.Message;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.CommentService;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.TimelineService;
import com.quest.etna.service.UserMessageRatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("comments")
public class CommentController {

    private final UserMessageRatingService userMessageRatingService;
    private final UserMessageRatingMapper userMessageRatingMapper;
    private final MessageService messageService;
    private final MessageMapper messageMapper;
    private final UserMapper userMapper;
    private final CommentService commentService;
    private final CommentMapper commentMapper;
    private final SecurityProvider securityProvider;
    private final TimelineService timelineService;

    @PostMapping("/messages/{messageId}")
    @ResponseStatus(HttpStatus.CREATED)
    public CommentDTO create(@RequestBody CommentDTO dto, @PathVariable Long messageId) {
        String dtoContent = dto.getContent();
        if (dtoContent == null || dtoContent.isEmpty() || dtoContent.isBlank()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Content must be defined");
        }
        Comment comment = commentMapper.objFromDto(dto);
        Message targetMessage = messageService.findById(messageId, true);
        comment.setMessage(targetMessage);
        comment.setUser(securityProvider.getAuthedUserModel());
        Comment createdComment = commentService.create(comment);
//        if (null != createdComment) {
//            timelineService.create(new Timeline(null, targetMessage, Action.COMMENTED, securityProvider.getAuthedUserModel()));
//        }
        return commentMapper.objToDto(createdComment);
    }

    @PutMapping("/{commentId}")
    @PreAuthorize("@commentService.findById(#commentId, true).getUser().getId()==authentication.id or hasRole('ROLE_ADMIN')")
    public CommentDTO update(@RequestBody CommentDTO dto, @PathVariable Long commentId) {
        dto.setId(commentId);
        return commentMapper.objToDto(commentService.update(commentMapper.objFromDto(dto)));
    }

    @GetMapping("{id}")
    public CommentDTO get(@PathVariable Long id) {
        return commentMapper.objToDto(commentService.findById(id, true));
    }

    @GetMapping("/messages/{messageId}")
    public List<CommentDTO> findAllByMessage(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                             @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                             @PathVariable Long messageId) {
        return commentMapper.objListToDtoList(commentService.findAllByMessage(messageId, PageRequest.of(page, perPage,
                Sort.by("creationDate"))));
    }

    @GetMapping("/_search")
    public List<CommentDTO> findAllByContent(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                             @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                             @RequestParam(value = "keyword", required = true) String keyword) {
        return commentMapper.objListToDtoList(commentService.findAllByContent(keyword, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("@commentService.findById(#id, true).getUser().getId()==authentication.id or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", commentService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }

}
