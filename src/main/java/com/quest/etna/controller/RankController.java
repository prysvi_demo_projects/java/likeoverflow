package com.quest.etna.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.quest.etna.dto.UserPromoNoteDTO;
import com.quest.etna.mapper.UserPromoNoteMapper;
import com.quest.etna.model.Promotion;
import com.quest.etna.model.UserPromoNote;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.EtnaService;
import com.quest.etna.service.PromotionService;
import com.quest.etna.service.UserPromoNoteService;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/rank")
public class RankController {
    private final UserPromoNoteService userPromoNoteService;
    private final PromotionService promotionService;
    private final SecurityProvider securityProvider;
    private final UserPromoNoteMapper userPromoNoteMapper;
    private final EtnaService etnaService;

    @GetMapping("/notes/promo")
    public NotesRankResponse getAllNotes(
            @RequestParam(required = false) Long promoId,
            @RequestParam(value = "reload", required = false, defaultValue = "false") boolean reload,
            @RequestParam(value = "sortDir", required = false, defaultValue = "ASC") Sort.Direction sortDir,
            @RequestParam(value = "perPage", required = false, defaultValue = "500") int perPage,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        List<UserPromoNote> foundNotes;
        Promotion promotion;
        PageRequest pageable = PageRequest.of(page, perPage, Sort.by(sortDir, "note"));
        Promotion currentUserPromo = securityProvider.getAuthedUserModel().getPromotion();
        if (null == promoId && null == currentUserPromo) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,
                    "User dont have promotion, use param promoId or assign promo to user");
        }
        promotion = null != promoId ? promotionService.findById(promoId, true) : currentUserPromo;

        if (reload) {
            if (!promotion.isCanBeReloaded()) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You can't reload promotion notes now," +
                        " promo can be reloaded after at " + promotion.getCanReloadAfterAt());
            }
            try {
                foundNotes = etnaService.asyncReloadPromoUsers(promotion, pageable);
            } catch (Exception e) {
                foundNotes = userPromoNoteService.findAllByPromoId(promotion.getId(), pageable);
            }
        } else {
            foundNotes = userPromoNoteService.findAllByPromoId(promotion.getId(), pageable);
        }

        return new NotesRankResponse(promotion, userPromoNoteMapper.objListToDtoList(foundNotes));
    }

    @Data
    @NoArgsConstructor
    public static class NotesRankResponse {
        private boolean canReload;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private Date lastReloadAt;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private Date canReloadAfterAt;
        private List<UserPromoNoteDTO> notes;

        public NotesRankResponse(Promotion promotion, List<UserPromoNoteDTO> notes) {
            this.notes = notes;
            this.lastReloadAt = promotion.getLastReloadAt();
            this.canReloadAfterAt = promotion.getCanReloadAfterAt();
            this.canReload = promotion.isCanBeReloaded();
        }
    }

}
