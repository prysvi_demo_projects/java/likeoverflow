package com.quest.etna.controller;

import com.quest.etna.dto.GradeDTO;
import com.quest.etna.mapper.GradeMapper;
import com.quest.etna.service.GradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("grades")
public class GradeController {

    private final GradeService gradeService;

    private final GradeMapper gradeMapper;

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GradeDTO create(@RequestBody GradeDTO dto) {
        return gradeMapper.objToDto(gradeService.create(gradeMapper.objFromDto(dto)));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public GradeDTO update(@RequestBody GradeDTO dto, @PathVariable Long id) {
        dto.setId(id);
        return gradeMapper.objToDto(gradeService.update(gradeMapper.objFromDto(dto)));
    }

    @GetMapping("{id}")
    public GradeDTO get(@PathVariable Long id) {
        return gradeMapper.objToDto(gradeService.findById(id, true));
    }

    @GetMapping("")
    public List<GradeDTO> findAll(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                  @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return gradeMapper.objListToDtoList(gradeService.findAll(PageRequest.of(page, perPage)));
    }

    @GetMapping("/_search")
    public List<GradeDTO> findAllByName(@RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
                                        @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                        @RequestParam(value = "keyword", required = true) String keyword) {
        return gradeMapper.objListToDtoList(gradeService.findAllByName(keyword, PageRequest.of(page, perPage)));
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        HashMap<String, Boolean> response = new HashMap<>() {
            {
                put("success", gradeService.delete(id));
            }
        };
        return ResponseEntity.ok(response);
    }
}
