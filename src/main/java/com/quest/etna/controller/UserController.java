package com.quest.etna.controller;

import com.quest.etna.dto.UserDTO;
import com.quest.etna.mapper.UserMapper;
import com.quest.etna.model.User;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.security.RolesAllowed;
import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
@RolesAllowed({"ROLE_USER", "ROLE_ADMIN"})
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final SecurityProvider securityProvider;
    private final BCryptPasswordEncoder passwordEncoder;

    @GetMapping("/{id}")
    public UserDTO getById(@PathVariable int id) {
        return userMapper.objToDto(userService.findById(id, true));
    }

    @GetMapping("")
    public List<UserDTO> getAll(
            @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
            @RequestParam(value = "page", required = false, defaultValue = "0") int page
    ) {
        if (securityProvider.authedHasRole("ROLE_ADMIN")) {
            return userMapper.objListToDtoList(userService.findAll(PageRequest.of(page, perPage)));
        } else {
            return userMapper.objListToDtoList(userService.findAllEnabled(true, PageRequest.of(page, perPage)));
        }
    }

    @PutMapping("{id}")
    @PreAuthorize("#id == authentication.id or hasRole('ROLE_ADMIN')")
    public UserDTO update(@RequestBody UserDTO userDTO, @PathVariable Long id) {
        if (!securityProvider.getAuthedUserRoles().contains("ROLE_ADMIN")) {
            if (userDTO.getRole() != null && securityProvider.getAuthedUserModel().getRole() != userDTO.getRole()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only admin can change your role!!!");
            }
        }
        User foundUser = userService.findById(id, true);
        if (userDTO.getUsername() != null && !foundUser.getUsername().equals(userDTO.getUsername())
                && null != userService.findByUserName(userDTO.getUsername(), false)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Username with this name already exists!!!");
        }
        userDTO.setId(id);
        if (userDTO.getPassword() != null) {
            userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        }
        User user = userMapper.objFromDto(userDTO);
        user.setEnabled(true);
        return userMapper.objToDto(userService.update(user, false));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("#id == authentication.id or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable int id) {
        User user = userService.findById(id, true);
        user.setEnabled(false);
        HashMap<String, String> response = new HashMap<>() {
            {
                //put("success", userService.delete(id) ? "TRUE" : "FALSE");
                put("success", !userService.update(user, false).isEnabled() ? "TRUE" : "FALSE");
            }
        };
        return ResponseEntity.ok(response);
    }
}
