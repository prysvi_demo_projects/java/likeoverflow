package com.quest.etna.controller;

import com.quest.etna.dto.ValidationUnitDTO;
import com.quest.etna.mapper.ValidationUnitActivityMapper;
import com.quest.etna.mapper.ValidationUnitMapper;
import com.quest.etna.model.ValidationUnit;
import com.quest.etna.service.ValidationUnitActivityService;
import com.quest.etna.service.ValidationUnitService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;

@RequiredArgsConstructor
@RestController
@RequestMapping("uv")
public class ValidationUnitController {

    private final ValidationUnitService uvService;
    private final ValidationUnitActivityService uvActivityService;

    private final ValidationUnitMapper validationUnitMapper;
    private final ValidationUnitActivityMapper validationUnitActivityMapper;

    @GetMapping("/_search")
    public ArrayList<ValidationUnitDTO> findByName(@RequestParam(name = "keyword", required = false) String keyword) {
        HashSet<ValidationUnit> found = new HashSet<>();
        Pageable unpaged = Pageable.unpaged();
        if (keyword != null) {
            found.addAll(uvService.findAllByNameContains(keyword, unpaged));
            found.addAll(uvService.findAllByLongNameContains(keyword, unpaged));
        } else {
            found.addAll(uvService.findAll(unpaged));
        }
        return validationUnitMapper.objListToDtoList(new ArrayList<>(found));
    }

}
