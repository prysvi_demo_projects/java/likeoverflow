package com.quest.etna.controller;

import com.quest.etna.dto.QuestionDTO;
import com.quest.etna.mapper.QuestionMapper;
import com.quest.etna.mapper.UserMapper;
import com.quest.etna.model.Message;
import com.quest.etna.model.Question;
import com.quest.etna.model.User;
import com.quest.etna.model.ValidationStatus;
import com.quest.etna.model.ValidationUnitActivity;
import com.quest.etna.security.SecurityProvider;
import com.quest.etna.service.MessageService;
import com.quest.etna.service.PromotionService;
import com.quest.etna.service.QuestionService;
import com.quest.etna.service.ReputationService;
import com.quest.etna.service.ValidationUnitActivityService;
import com.quest.etna.service.ValidationUnitService;
import java.util.HashMap;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@RestController
@RequestMapping("questions")
public class QuestionController {

  public final static String QUESTION_CACHE = "questions";
  private final QuestionService questionService;
  private final QuestionMapper questionMapper;
  private final UserMapper userMapper;
  private final SecurityProvider securityProvider;
  private final PromotionService promotionService;
  private final ReputationService reputationService;
  private final MessageService messageService;
  private final ValidationUnitActivityService uvActivityService;
  private final ValidationUnitService uvService;

  @PostMapping("")
  @ResponseStatus(HttpStatus.CREATED)
  @CacheEvict(value = QUESTION_CACHE, allEntries = true)
  public QuestionDTO create(@RequestBody QuestionDTO dto) {
    User authedUser = securityProvider.getAuthedUserModel();

    if (dto.getUvActivity() == null) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "UV must not be null");
    }
    if (dto.getQuestionMessage() == null) {
      throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Message must not be null");
    }

    securityProvider.checkEnoughReputationOrRise(4);
    reputationService.refreshUserReputation(authedUser, -4);

    dto.setStatus(ValidationStatus.VALIDATED);
    Question question = questionMapper.objFromDto(dto);
    question.setUser(authedUser);
    question.setPromotion(authedUser.getPromotion());
    question.setViews(0);
    if (question.getUvActivity() != null) {
      question.setUvActivity(uvActivityService.findById(question.getUvActivity().getId(), true));
    }
    Message questionMessage = question.getQuestionMessage();
    questionMessage.setUser(authedUser);

    questionMessage = messageService.create(question.getQuestionMessage());
    question.setQuestionMessage(questionMessage);
    Question result = questionService.create(question);
    if (result == null) // rollback if question net created
    {
      messageService.delete(question.getQuestionMessage().getId());
    } else {
      questionMessage.setQuestion(result);
      messageService.update(questionMessage);
    }
    return questionMapper.objToDto(result);
  }

  @CacheEvict(value = QUESTION_CACHE, allEntries = true)
  @PutMapping("{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN') or @questionService.findById(#id,true).getUser().id==authentication.id")
  public QuestionDTO update(@RequestBody QuestionDTO dto, @PathVariable Long id) {
    Question found = questionService.findById(id, true);
    dto.setId(id);
    Question updatedQuestion = questionMapper.objFromDto(dto);
    ValidationUnitActivity updatedQuestionActivity = updatedQuestion.getUvActivity();
    if (updatedQuestionActivity != null && !updatedQuestionActivity.getId()
        .equals(found.getUvActivity().getId())) {
      updatedQuestion
          .setUvActivity(uvActivityService.findById(updatedQuestionActivity.getId(), true));
    }
    return questionMapper.objToDto(questionService.update(updatedQuestion));
  }


  @CacheEvict(value = QUESTION_CACHE, allEntries = true)
  @GetMapping("{id}")
  public QuestionDTO get(@PathVariable Long id) {
    Question found = questionService.findById(id, true);
    found.setViews(found.getViews() + 1);
    return questionMapper.objToDto(questionService.update(found));
  }

  @Cacheable(cacheNames = {
      QUESTION_CACHE}, key = "new org.springframework.cache.interceptor.SimpleKey('all',#perPage,#page)")
  @GetMapping("")
  public Page<QuestionDTO> findAll(
      @RequestParam(value = "sortDir", required = false, defaultValue = "DESC") Sort.Direction sortDir,
      @RequestParam(value = "sortField", required = false, defaultValue = "creationDate") String sortField,
      @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
    return questionService.findAll(PageRequest.of(page, perPage,
        Sort.by(sortDir, sortField))).map(questionMapper::objToDto);
  }

  @GetMapping("/_search")
  @Cacheable(cacheNames = {
      QUESTION_CACHE}, key = "new org.springframework.cache.interceptor.SimpleKey(#title,#uvId,#perPage,#page)")
  public List<QuestionDTO> findAllByTitleContains(
      @RequestParam(value = "perPage", required = false, defaultValue = "100") int perPage,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
      @RequestParam(value = "title", required = false) String title,
      @RequestParam(value = "uvId", required = false) Long uvId
  ) {
    ValidationUnitActivity unitActivity = null;
    if (uvId != null) {
      unitActivity = uvActivityService.findById(uvId, true);
    }
    List<Question> allByCriteria = questionService
        .findAllByCriteria(title, unitActivity, PageRequest.of(page, perPage));
    return questionMapper.objListToDtoList(allByCriteria);
  }

  @CacheEvict(value = QUESTION_CACHE, allEntries = true)
  @DeleteMapping("{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN') or @questionService.findById(#id,true).getUser().id==authentication.id")
  public ResponseEntity<?> delete(@PathVariable long id) {
    if (messageService.findAllByQuestionPage(id, Pageable.unpaged()).getContent().size() > 0
        && !securityProvider.authedHasRole("ROLE_ADMIN")) {
      throw new ResponseStatusException(HttpStatus.FORBIDDEN,
          "You cannot remove active Question with responses");
    }
    HashMap<String, Boolean> response = new HashMap<>() {
      {
        put("success", questionService.delete(id));
      }
    };
    return ResponseEntity.ok(response);
  }
}
