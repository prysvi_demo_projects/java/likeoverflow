package com.quest.etna.service;

import com.quest.etna.model.ChatMessage;
import com.quest.etna.repository.ChatMessageRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ChatMessageService {
    private final ModelOperationHelper modelOperationHelper;

    private final ChatMessageRepository chatMessageRepository;

    @Transactional(readOnly = true)
    public ChatMessage findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return chatMessageRepository.findById(id).orElse(null);
        } else {
            return chatMessageRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("ChatMessage %d not found",
                            id)));
        }
    }

    public ChatMessage create(ChatMessage obj) {
        return chatMessageRepository.save(obj);
    }

    public ChatMessage update(ChatMessage obj) {
        ChatMessage dbChatMessage = this.findById(obj.getId(), true);
        return chatMessageRepository.save(modelOperationHelper.secureUpdate(obj, dbChatMessage, false));
    }

    @Transactional(readOnly = true)
    public List<ChatMessage> findAll(Pageable pageable) {
        return chatMessageRepository.findAll(pageable).getContent();
    }


    public boolean delete(long id) {
        try {
            chatMessageRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
