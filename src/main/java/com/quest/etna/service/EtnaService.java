package com.quest.etna.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quest.etna.config.EtnaConnector;
import com.quest.etna.model.*;
import com.quest.etna.model.etna.EtnaNote;
import com.quest.etna.model.etna.EtnaPromo;
import com.quest.etna.model.etna.EtnaUser;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@EnableAsync
@EnableScheduling
@Slf4j
@Data
@Service
@RequiredArgsConstructor
public class EtnaService {
    private final String etnaApiUrl = "https://intra-api.etna-alternance.net";
    private final EtnaConnector etnaConnector;
    private final ObjectMapper mapper;
    private final PromotionService promotionService;
    private final UserService userService;
    private final ValidationUnitService uvService;
    private final ValidationUnitActivityService uvActivityService;
    private final UserPromoNoteService userPromoNoteService;
    private final FileSystemStorageService fileSystemStorageService;
    private Random random = new Random();

    @Async
    @Transactional
    @Scheduled(cron = "${cron.expression}", zone = "Europe/Paris")
    public void checkPromosTask() {
        long start = System.currentTimeMillis();
        log.info("Checking Etna Promos for changes... ");
        findAllEtnaPromos().forEach(etnaPromo -> {
            log.info("Checking Etna Promo... " + etnaPromo.getWall_name());
            Promotion promotion = promotionService.findById(etnaPromo.getId(), false);
            if (promotion == null) {
                promotion = promotionService.getOrCreate(etnaPromo);
                updatePromoUsers(promotion);
            } else {
                List<User> users = promotion.getUsers();
                if (users != null && !users.isEmpty()) {
                    for (int i = 0; i < 2; i++) {
                        if (reloadUserNotes(users.get(random.nextInt(users.size())))) {
                            updatePromoUsers(promotion);
                            break;
                        }
                    }
                } else {
                    updatePromoUsers(promotion);
                }
            }
        });
        long end = System.currentTimeMillis();
        log.info("Total reload all promo process time {} milliseconds", (end - start));
    }

    public void updatePromoUsers(Promotion promotion) {
        long start = System.currentTimeMillis();
        findEtnaPromoUsers(String.valueOf(promotion.getId())).forEach(etnaUser -> {
            User foundUser = getByEtnaUserOrCreate(etnaUser, promotion);
            if (!foundUser.getPromotion().getId().equals(promotion.getId())) {
                foundUser.setPromotion(promotion);
                foundUser = userService.update(foundUser, true);
            }
            reloadUserNotes(foundUser);
        });
        promotion.setUpdatedDate(new Date());
        promotionService.update(promotion);
        long end = System.currentTimeMillis();
        log.debug("Total reload USERS in promo process time {} milliseconds", (end - start));
    }

    public User getByEtnaUserOrCreate(EtnaUser etnaUser, Promotion promotion) {
        if (etnaUser == null) {
            return null;
        }
        String userEmail = etnaUser.getEmail() != null ? etnaUser.getEmail() : etnaUser.getLogin() + "@etna-alternance.net";
        User found = userService.findByUserName(userEmail, false);
        if (null == found) {
            String etnaLogin = etnaUser.getLogin() != null ? etnaUser.getLogin() : etnaUser.getEmail();
            if (etnaLogin == null && (userEmail.isEmpty() || !userEmail.contains("@"))) {
                return null;
            } else if (etnaLogin == null) {
                etnaLogin = userEmail.substring(0, userEmail.indexOf("@"));
            }
            found = userService.create(new User(userEmail, etnaLogin, UserRole.ROLE_USER,
                    loadUserPhoto(etnaLogin), false, promotion));
        }
        return found;
    }

    public User getByEtnaLogin(String etnaLogin) {
        if (etnaLogin == null || etnaLogin.isEmpty() || etnaLogin.contains("@")) {
            return null;
        }
        User foundUser = userService.findByUserName(etnaLogin + "@etna-alternance.net", false);
        if (null == foundUser) {
            foundUser = reloadUserFromEtnaByLogin(etnaLogin);
        }
        return foundUser;
    }

    public User getByEmail(String email) {
        if (email == null || email.isEmpty() || !email.contains("@")) {
            return null;
        }
        User foundUser = userService.findByUserName(email, false);
        if (null == foundUser) {
            foundUser = reloadUserFromEtnaByLogin(email.substring(0, email.indexOf("@")));
        }
        return foundUser;
    }

    public String loadUserPhoto(String etnaLogin) {
        try {
            Flux<DataBuffer> photoBuffer = etnaConnector.getConnectedWebClient().get().uri("https://auth.etna-alternance.net/api/users/"
                    + etnaLogin + "/photo")
                    .retrieve().bodyToFlux(DataBuffer.class);
            return "${api}" + fileSystemStorageService.storeUserDataBufferFile(photoBuffer, etnaLogin);
        } catch (IOException e) {
            log.error("could not load user photo " + etnaLogin);
            return "";
        }
    }


    public List<UserPromoNote> asyncReloadPromoUsers(Promotion promotion, Pageable pageable) {
        updatePromoUsers(promotion);
        return userPromoNoteService.findAllByPromoId(promotion.getId(),
            pageable);
    }

    public Promotion importPromotionById(String id, boolean initUsers) {
        EtnaPromo etnaPromo = findEtnaPromoById(id);
        Promotion promotion = promotionService.findById(etnaPromo.getId(), false);
        if (promotion == null) {
            promotion = promotionService.getOrCreate(etnaPromo);
        }
        if (initUsers) {
            updatePromoUsers(promotion);
        }
        return promotion;
    }

    public User reloadUserFromEtnaByLogin(String etnaLogin) {
        if (etnaLogin == null) {
            return null;
        }
        EtnaUser importedUser = findEtnaUserByLogin(etnaLogin);
        if (null == importedUser) {
            return null;
        }

        Promotion userPromo = findEtnaPromosByLogin(etnaLogin).stream().map(etnaPromo -> {
            Promotion promo = promotionService.findById(etnaPromo.getId(), false);
            if (promo == null) {
                promo = promotionService.getOrCreate(etnaPromo);
            }
            return promo;
        }).sorted(Comparator.comparing(Promotion::getLearningEnd).reversed())
                .collect(Collectors.toCollection(ArrayList::new)).stream().findFirst().orElse(null);
        return getByEtnaUserOrCreate(importedUser, userPromo);
    }


    public boolean reloadUserNotes(User foundUser) {
        boolean reloaded = false;
        ArrayList<Double> notes = new ArrayList<>();
        Promotion promotion = foundUser.getPromotion();
        if (null == promotion) {
            return false;
        }
        findUserNotesByPromoAndLogin(String.valueOf(promotion.getId()), foundUser.getEtnaLogin())
                .forEach(etnaNote -> {
                    ValidationUnit uv = uvService.findByName(etnaNote.getUv_name(), false);
                    if (null == uv) {
                        uv = uvService.create(new ValidationUnit(null, etnaNote.getUv_name(),
                                etnaNote.getUv_long_name(), null));
                    }

                    ValidationUnitActivity uvActivity = uvActivityService.
                            findAllByValidationUnitIdAndActivityName(uv.getId(), etnaNote.getActivity_name());

                    if (null == uvActivity) {
                        uvActivityService.create(new ValidationUnitActivity(null,
                                etnaNote.getActivity_name(), uv));
                    }
                    if (etnaNote.getStudent_mark() != null && !etnaNote.getUv_name().contains("FDI-")) {
                        notes.add(etnaNote.getStudent_mark());
                    }
                });
        Double midNote = notes.stream().mapToDouble(m -> m).sum() / (long) notes.size();
        if (midNote.isNaN()) {
            midNote = 0.0;
        }
        UserPromoNote usrPromoNote = userPromoNoteService.findById(new UserPromoNoteId(foundUser.getId(),
                promotion.getId()), false);

        if (null == usrPromoNote) {
            userPromoNoteService.create(new UserPromoNote(foundUser, promotion, midNote));
            reloaded = true;
        } else if (!midNote.equals(usrPromoNote.getNote())) {
            usrPromoNote.setNote(midNote);
            userPromoNoteService.update(usrPromoNote, false);
            reloaded = true;
        }
        return reloaded;
    }

    public EtnaUser findEtnaUserByLogin(String etnaLogin) {
        try {
            return etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/users/" + etnaLogin).retrieve()
                    .bodyToMono(EtnaUser.class).block();
        } catch (WebClientRequestException e) {
            return null;
        }
    }

    public EtnaPromo findEtnaPromoById(String promoId) {
        try {
            TrombiResponse res = etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/trombi/" + promoId).retrieve()
                    .bodyToMono(TrombiResponse.class).block();

            if (res != null) {
                return res.getTerm();
            } else {
                return null;
            }
        } catch (WebClientRequestException e) {
            return null;
        }
    }

    public List<EtnaUser> findEtnaPromoUsers(String promoId) {
        try {
            TrombiResponse res = etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/trombi/" + promoId).retrieve()
                    .bodyToMono(TrombiResponse.class).block();

            ArrayList<EtnaUser> resList = new ArrayList<>();
            if (res != null && res.students != null && res.students.size() > 0) {
                resList.addAll(res.students);
                resList.sort(Comparator.comparingInt(EtnaUser::getId));
            }
            return resList;
        } catch (WebClientRequestException e) {
            return Collections.emptyList();
        }
    }

    public List<EtnaPromo> findEtnaPromosByLogin(String etnaLogin) {
        try {
            EtnaPromo[] res = etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/promo?login=" + etnaLogin).retrieve()
                    .bodyToMono(EtnaPromo[].class).block();

            ArrayList<EtnaPromo> resList = new ArrayList<EtnaPromo>();
            if (res != null && res.length > 0) {
                resList.addAll(Arrays.asList(res));
                resList.sort(Comparator.comparingLong(EtnaPromo::getId));
            }
            return resList;
        } catch (WebClientRequestException e) {
            return Collections.emptyList();
        }
    }

    public List<EtnaPromo> findAllEtnaPromos() {
        try {
            String json = etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/trombi").retrieve()
                    .bodyToMono(String.class).block();
            HashMap<String, List<EtnaPromo>> promosMap = mapper.readValue(json, new TypeReference<HashMap<String, List<EtnaPromo>>>() {
            });
            return promosMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        } catch (WebClientRequestException | JsonProcessingException e) {
            return Collections.emptyList();
        }
    }


    public List<EtnaNote> findUserNotesByPromoAndLogin(String promoId, String etnaLogin) {
        try {
            EtnaNote[] res = etnaConnector.getConnectedWebClient().get()
                    .uri(etnaApiUrl + "/terms/" + promoId + "/students/" + etnaLogin + "/marks")
                    .retrieve()
                    .bodyToMono(EtnaNote[].class).block();

            ArrayList<EtnaNote> resList = new ArrayList<EtnaNote>();
            if (res != null && res.length > 0) {
                resList.addAll(Arrays.asList(res));
            }
            return resList;
        } catch (WebClientRequestException e) {
            return Collections.emptyList();
        }
    }


    @NoArgsConstructor
    @Data
    public static class TrombiResponse {
        private List<EtnaUser> students;
        private EtnaPromo term;
    }
}
