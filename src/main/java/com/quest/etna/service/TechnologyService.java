package com.quest.etna.service;

import com.quest.etna.model.Technology;
import com.quest.etna.repository.TechnologyRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TechnologyService {
    private final ModelOperationHelper modelOperationHelper;

    private final TechnologyRepository technologyRepository;

    @Transactional(readOnly = true)
    public Technology findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return technologyRepository.findById(id).orElse(null);
        } else {
            return technologyRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Technology %d not found",
                            id)));
        }
    }

    public Technology create(Technology obj) {
        return technologyRepository.save(obj);
    }

    public Technology update(Technology obj) {
        Technology dbTechnology = this.findById(obj.getId(), true);
        return technologyRepository.save(modelOperationHelper.secureUpdate(obj, dbTechnology, false));
    }

    @Transactional(readOnly = true)
    public List<Technology> findAll(Pageable pageable) {
        return technologyRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<Technology> findAllByName(String name, Pageable pageable) {
        return technologyRepository.findAllByName(name, pageable);
    }

    public boolean delete(long id) {
        try {
            technologyRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
