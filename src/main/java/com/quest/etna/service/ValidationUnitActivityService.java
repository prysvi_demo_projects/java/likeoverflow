package com.quest.etna.service;

import com.quest.etna.model.ValidationUnitActivity;
import com.quest.etna.repository.ValidationUnitActivityRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.Data;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@Data
public class ValidationUnitActivityService {
    private final ModelOperationHelper modelOperationHelper;

    private final ValidationUnitActivityRepository validationUnitRepository;

    @Transactional(readOnly = true)
    public ValidationUnitActivity findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return validationUnitRepository.findById(id).orElse(null);
        } else {
            return validationUnitRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("ValidationUnitActivity %d not found",
                            id)));
        }
    }

    public ValidationUnitActivity create(ValidationUnitActivity obj) {
        return validationUnitRepository.save(obj);
    }

    public ValidationUnitActivity update(ValidationUnitActivity obj) {
        ValidationUnitActivity dbValidationUnitActivity = this.findById(obj.getId(), true);
        return validationUnitRepository.save(modelOperationHelper.secureUpdate(obj, dbValidationUnitActivity, false));
    }

    @Transactional(readOnly = true)
    public List<ValidationUnitActivity> findAll(Pageable pageable) {
        return validationUnitRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<ValidationUnitActivity> findAllByName(String name, Pageable pageable) {
        return validationUnitRepository.findAllByName(name, pageable);
    }

    public boolean delete(long id) {
        try {
            validationUnitRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


    public ValidationUnitActivity findAllByValidationUnitIdAndActivityName(Long id, String activity_name) {
        return validationUnitRepository.findAllByValidationUnitIdAndActivityName(id, activity_name).stream().findFirst().orElse(null);
    }
}
