package com.quest.etna.service;

import com.quest.etna.model.Address;
import com.quest.etna.repository.AddressRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class AddressService {

    private final ModelOperationHelper modelOperationHelper;
    private final AddressRepository addressRepository;


    @Transactional(readOnly = false)
    public Address create(Address reqAddress) {
        reqAddress.setCreationDate(new Date());
        reqAddress.setUpdatedDate(new Date());
        return addressRepository.save(reqAddress);
    }

    @Transactional(readOnly = false)
    public Address update(Address reqAddress, boolean applyNullValues) {
        Address dbAddress = addressRepository.findById(reqAddress.getId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address %d not found",
                        reqAddress.getId())));
        return addressRepository.save(modelOperationHelper.secureUpdate(reqAddress, dbAddress, applyNullValues));
    }

    @Transactional(readOnly = true)
    public List<Address> findAll(Pageable pageable) {
        return addressRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<Address> findAllByUserId(long userId, Pageable pageable) {
        return addressRepository.findAllByUserId(userId, pageable).getContent();
    }

    public boolean delete(long id) {
        try {
            addressRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }

    @Transactional(readOnly = true)
    public Address findById(long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return addressRepository.findById(id).orElse(null);
        } else {
            return addressRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Address %d not found", id)));
        }
    }
}
