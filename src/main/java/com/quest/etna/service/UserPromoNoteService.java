package com.quest.etna.service;

import com.quest.etna.model.UserPromoNote;
import com.quest.etna.model.UserPromoNoteId;
import com.quest.etna.repository.UserPromoNoteRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Data
@Service
public class UserPromoNoteService implements IService<UserPromoNote, UserPromoNoteId> {
    private final UserPromoNoteRepository repository;
    private final ModelOperationHelper modelOperationHelper;

    @Override
    public UserPromoNote findById(UserPromoNoteId id, boolean throwNotFound) {
        if (throwNotFound) {
            return repository.findById(id).orElseThrow(() ->
                    new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("UserPromoNote with id %d not found",
                            id)));
        } else {
            return repository.findById(id).orElse(null);
        }
    }

    public List<UserPromoNote> findAllByPromoId(Long promoId, Pageable pageable) {
        return repository.findAllByPromoId(promoId, pageable).getContent();
    }

    @Override
    public UserPromoNote create(UserPromoNote obj) {
        return repository.save(obj);
    }

    @Override
    public UserPromoNote update(UserPromoNote obj, boolean applyNullValues) {
        return repository.save(modelOperationHelper.secureUpdate(obj, this.findById(obj.getId(), true),
                applyNullValues));
    }

    @Override
    public boolean delete(UserPromoNote oToDelete) {
        repository.deleteById(oToDelete.getId());
        return findById(oToDelete.getId(), false) == null;
    }


}
