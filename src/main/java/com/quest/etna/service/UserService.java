package com.quest.etna.service;

import com.quest.etna.model.Promotion;
import com.quest.etna.model.User;
import com.quest.etna.model.etna.EtnaUser;
import com.quest.etna.repository.UserRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class UserService {

    private final ModelOperationHelper modelOperationHelper;

    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public User findByUserName(String username, boolean throwNotFound) {
        User foundUser = userRepository.findByUsername(username);
        if (null == foundUser && throwNotFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User %s not found", username));
        }
        return foundUser;
    }

    @Transactional(readOnly = true)
    public User findById(long id, boolean throwNotFound) {
        if (throwNotFound) {
            return userRepository.findById(id).orElseThrow(() ->
                    new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d not found", id)));
        } else {
            return userRepository.findById(id).orElse(null);
        }
    }


    @Transactional(readOnly = true)
    public List<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<User> findAllEnabled(boolean isEnabled, Pageable pageable) {
        return userRepository.findAllByEnabled(isEnabled, pageable).getContent();
    }

    @Transactional(readOnly = false)
    public User create(User reqUser) {
        reqUser.setCreationDate(new Date());
        reqUser.setUpdatedDate(new Date());
        return userRepository.save(reqUser);
    }

//    public User getOrCreate(EtnaUser etnaUser, Promotion promotion) {
//        String userEmail = etnaUser.getEmail() != null ? etnaUser.getEmail() : etnaUser.getLogin() + "@etna-alternance.net";
//        User found = userRepository.findByUsername(userEmail);
//        if (null == found) {
//            found = userRepository.save(new User(etnaUser, promotion));
//        }
//        return found;
//    }

    @Transactional(readOnly = false)
    public User update(User reqUser, boolean applyNullValues) {
        User dbUser = this.findById(reqUser.getId(), true);
        var res = modelOperationHelper.secureUpdate(reqUser, dbUser, applyNullValues);
        return userRepository.save(res);
    }

    public boolean delete(long id) {
        try {
            userRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }

    public List<User> findAllByCreationDateBetween(Date start, Date end) {
        return userRepository.findAllByCreationDateBetween(start, end);
    }

}
