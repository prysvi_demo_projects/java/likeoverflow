package com.quest.etna.service;

import com.quest.etna.model.Promotion;
import com.quest.etna.model.etna.EtnaPromo;
import com.quest.etna.repository.PromotionRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PromotionService {
    private final ModelOperationHelper modelOperationHelper;

    private final PromotionRepository technologyRepository;

    @Transactional(readOnly = true)
    public Promotion findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return technologyRepository.findById(id).orElse(null);
        } else {
            return technologyRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Promotion %d not found",
                            id)));
        }
    }

    public Promotion create(Promotion obj) {
        return technologyRepository.save(obj);
    }

    public Promotion getOrCreate(EtnaPromo etnaPromo) {
        return technologyRepository.findById(etnaPromo.getId())
                .orElse(technologyRepository.save(new Promotion(etnaPromo)));
    }

    public Promotion update(Promotion obj) {
        Promotion dbPromotion = this.findById(obj.getId(), true);
        return technologyRepository.save(modelOperationHelper.secureUpdate(obj, dbPromotion, false));
    }

    @Transactional(readOnly = true)
    public List<Promotion> findAll(Pageable pageable) {
        return technologyRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<Promotion> findAllByName(String name, Pageable pageable) {
        return technologyRepository.findAllByName(name, pageable);
    }

    public boolean delete(long id) {
        try {
            technologyRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
