package com.quest.etna.service;

public interface IService<O,I> {
    public O findById(I id, boolean throwNotFound);
    public O create(O obj);
    public O update(O obj, boolean applyNullValues);
    public boolean delete(O oToDelete);
}
