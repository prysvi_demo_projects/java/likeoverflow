package com.quest.etna.service;

import com.quest.etna.model.Timeline;
import com.quest.etna.repository.TimelineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TimelineService {


    private final TimelineRepository timelineRepository;

    @Transactional(readOnly = true)
    public Timeline findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return timelineRepository.findById(id).orElse(null);
        } else {
            return timelineRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Timeline %d not found",
                            id)));
        }
    }

    public Timeline create(Timeline obj) {
        return timelineRepository.save(obj);
    }


    @Transactional(readOnly = true)
    public List<Timeline> findAll(Pageable pageable) {
        return timelineRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public Page<Timeline> findAllByUser(Long userId, Pageable pageable) {
        return timelineRepository.findAllByUserId(userId, pageable);
    }

    @Transactional(readOnly = true)
    public Page<Timeline> findAllByMessage(Long messageId, Pageable pageable) {
        return timelineRepository.findAllByMessageId(messageId, pageable);
    }
}
