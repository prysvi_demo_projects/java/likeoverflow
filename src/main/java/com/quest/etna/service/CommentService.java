package com.quest.etna.service;

import com.quest.etna.model.Comment;
import com.quest.etna.repository.CommentRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CommentService {
    private final ModelOperationHelper modelOperationHelper;

    private final CommentRepository commentRepository;

    @Transactional(readOnly = true)
    public Comment findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return commentRepository.findById(id).orElse(null);
        } else {
            return commentRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Comment %d not found",
                            id)));
        }
    }

    public Comment create(Comment obj) {
        return commentRepository.save(obj);
    }

    public Comment update(Comment obj) {
        return commentRepository.save(modelOperationHelper.secureUpdate(obj, this.findById(obj.getId(),
                true), false));
    }

    @Transactional(readOnly = true)
    public List<Comment> findAll(Pageable pageable) {
        return commentRepository.findAll(pageable).getContent();
    }

    @Transactional(readOnly = true)
    public List<Comment> findAllByContent(String name, Pageable pageable) {
        return commentRepository.findAllByContentContainsText(name, pageable);
    }

    @Transactional(readOnly = true)
    public List<Comment> findAllByMessage(Long messageId, Pageable pageable) {
        return commentRepository.findAllByMessage_Id(messageId, pageable).getContent();
    }

    public boolean delete(long id) {
        try {
            commentRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }


}
