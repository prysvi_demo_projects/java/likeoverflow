package com.quest.etna.service;

import com.quest.etna.model.Question;
import com.quest.etna.model.ValidationUnitActivity;
import com.quest.etna.repository.QuestionRepository;
import com.quest.etna.utils.ModelOperationHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@RequiredArgsConstructor
@Service
public class QuestionService {
    private final ModelOperationHelper modelOperationHelper;

    private final QuestionRepository questionRepository;

    @Transactional(readOnly = true)
    public Question findById(Long id, boolean throwNotFound) {
        if (!throwNotFound) {
            return questionRepository.findById(id).orElse(null);
        } else {
            return questionRepository.findById(id).orElseThrow(
                    () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Question %d not found",
                            id)));
        }
    }

    public Question create(Question obj) {
        return questionRepository.save(obj);
    }

    public Question update(Question obj) {
        Question dbQuestion = this.findById(obj.getId(), true);
        return questionRepository.save(modelOperationHelper.secureUpdate(obj, dbQuestion, false));
    }

    @Transactional(readOnly = true)
    public Page<Question> findAll(Pageable pageable) {
        return questionRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public List<Question> findAllByCriteria(String title, ValidationUnitActivity uvActivity, Pageable pageable) {

        HashSet<Question> questions = new HashSet<>();
        if (title != null) {
            questions.addAll(questionRepository.findAllByTitle(title, pageable));
        }

        if (uvActivity != null) {
            questions.addAll(questionRepository.findAllByUvActivity(uvActivity, pageable).getContent());
        }
        return new ArrayList<>(questions);
    }

    public List<Question> findAllByCreationDateBetween(Date start, Date end) {
        return questionRepository.findAllByCreationDateBetween(start, end);
    }


    public boolean delete(long id) {
        try {
            questionRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException ignored) {
            return false;
        }
    }
}
