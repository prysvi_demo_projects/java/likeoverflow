package com.quest.etna.service;

import com.quest.etna.model.UserMessageRating;
import com.quest.etna.model.UserMessageRatingId;
import com.quest.etna.repository.UserMessageRatingRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Data
@Service
@RequiredArgsConstructor
public class UserMessageRatingService {

    private final UserMessageRatingRepository userMessageRatingRepository;

    public UserMessageRating saveOrUpdate(UserMessageRating rating) {
        return userMessageRatingRepository.save(rating);
    }

    public UserMessageRating findByIdOrCreate(UserMessageRatingId userMessageRatingId) {
        return userMessageRatingRepository.findById(userMessageRatingId)
                .orElse(new UserMessageRating());
    }
}
