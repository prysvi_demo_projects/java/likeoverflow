package com.quest.etna.service;

import com.quest.etna.repository.FileStorageRepository;
import com.quest.etna.security.SecurityProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service("FileStorageService")
public class FileSystemStorageService implements FileStorageRepository {

    private final SecurityProvider securityProvider;
    @Value("${app.upload.upload-dir:uploads}")
    private String uploadDir;
    @Value("${app.upload.url-path:storage}")
    private String uploadUrlToDir;

    @Override
    public String store(MultipartFile file) {
        String etnaLogin = securityProvider.getAuthedUserDetails().getEtnaLogin();
        return storeUserMultipartFile(file, etnaLogin);
    }

    @Override
    public String storeUserMultipartFile(MultipartFile file, String etnaLogin) {
        if (null == file || file.isEmpty()) {
            throw new RuntimeException("Failed to store empty file.");
        }

        File upDir = new File(uploadDir);
        checkAndCreateDirectory(upDir);
        File userDir = new File(upDir.getAbsolutePath() + "/" + etnaLogin);
        checkAndCreateDirectory(userDir);
        try {
            File resultFile = new File(userDir.getAbsolutePath() + "/" + file.getOriginalFilename());
            file.transferTo(resultFile);
            return String.format("/%s/%s/%s", uploadUrlToDir, userDir.getName(), resultFile.getName());
        } catch (IOException e) {
            throw new RuntimeException("Cant create file " + file.getOriginalFilename());
        }
    }

    public String storeUserDataBufferFile(Flux<DataBuffer> dataBufferFlux, String etnaLogin) throws IOException {
        File upDir = new File(uploadDir);
        checkAndCreateDirectory(upDir);
        File userDir = new File(upDir.getAbsolutePath() + "/" + etnaLogin);
        checkAndCreateDirectory(userDir);
        Path profilePath = Path.of(userDir.getAbsolutePath() + "/profile.jpeg");
        Files.deleteIfExists(profilePath);
        Path resultFile = Files.createFile(profilePath);
        DataBufferUtils.write(dataBufferFlux, resultFile, StandardOpenOption.CREATE).block();
        return String.format("/%s/%s/%s", uploadUrlToDir, userDir.getName(), resultFile.getFileName().toString());

    }

    private void checkAndCreateDirectory(File upDir) {
        if (!upDir.exists() && !upDir.mkdir()) {
            throw new RuntimeException("Cant create dir " + upDir.getAbsolutePath());
        }
        if (!upDir.canRead()) {
            upDir.setReadable(true, false);
        }
        if (!upDir.canWrite()) {
            upDir.setWritable(true, false);
        }
    }


    @Override
    public void deleteAll() {

    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public boolean delete(String filename) {
        File file = new File(uploadDir + "/" + filename);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }
}
