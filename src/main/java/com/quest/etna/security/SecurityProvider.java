package com.quest.etna.security;

import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.security.jwt.JwtUserDetailsService;
import com.quest.etna.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SecurityProvider {
    private final JwtUserDetailsService jwtUserDetailsService;
    private final UserService userService;

    public void checkEnoughReputationOrRise(int needed) {
        if (getAuthedUserModel().getReputation() - needed < 0) {
            throw new ResponseStatusException(HttpStatus.PAYMENT_REQUIRED, "You dont have enough reputation to do that");
        }
    }

    public JwtUserDetails getAuthedUserDetails() {
        return jwtUserDetailsService.loadUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public User getAuthedUserModel() {
        return userService.findByUserName(getAuthedUserDetails().getUsername(), true);
    }

    public List<String> getAuthedUserRoles() {
        return getAuthedUserDetails().getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
    }

    public boolean authedHasRole(String role) {
        return getAuthedUserRoles().contains(role);
    }
}