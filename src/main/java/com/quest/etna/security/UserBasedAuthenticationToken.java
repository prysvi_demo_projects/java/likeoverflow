package com.quest.etna.security;

import com.quest.etna.model.JwtUserDetails;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class UserBasedAuthenticationToken extends UsernamePasswordAuthenticationToken {
    private Long id;
    private String username;
    private String etnaLogin;

    public UserBasedAuthenticationToken(JwtUserDetails jwtUserDetails) {
        super(jwtUserDetails.getUsername(), jwtUserDetails.getPassword(), jwtUserDetails.getAuthorities());
        this.id = jwtUserDetails.getId();
        this.username = jwtUserDetails.getUsername();
        this.etnaLogin = jwtUserDetails.getEtnaLogin();
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEtnaLogin() {
        return etnaLogin;
    }
}
