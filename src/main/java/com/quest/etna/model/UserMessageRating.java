package com.quest.etna.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "user_message_rating")
@IdClass(UserMessageRatingId.class)
public class UserMessageRating extends BaseModel {
    @Id
    Long userId;

    @Id
    Long messageId;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "user")
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "message")
    private Message message;

    @Enumerated(value = EnumType.ORDINAL)
    @Column()
    private Reaction reaction;
}