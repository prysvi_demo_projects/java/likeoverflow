package com.quest.etna.model;

import com.quest.etna.annotation.ReflectionUpdate;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@ToString(exclude = {"responses"})
@EqualsAndHashCode(callSuper = false, of = {"id"})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Question extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private User user;

    @Column()
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    private Integer views = 0;

    @Column(nullable = false, length = 100)
    private String title;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    @JoinColumn(name = "promotion", referencedColumnName = "id")
    private Promotion promotion;

//    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "uv_activity", referencedColumnName = "id")
    private ValidationUnitActivity uvActivity;


    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @Enumerated(value = EnumType.STRING)
    @Column()
    private ValidationStatus status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "question", referencedColumnName = "id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Message> responses;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.REFRESH)
    @JoinColumn(name = "question_message", referencedColumnName = "id")
    private Message questionMessage;
}