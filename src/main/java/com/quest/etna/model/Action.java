package com.quest.etna.model;

public enum Action {
    CREATED, REACTED, COMMENTED, UPDATED
}