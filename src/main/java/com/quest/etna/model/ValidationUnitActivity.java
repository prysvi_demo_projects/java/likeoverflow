package com.quest.etna.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = false, of = {"id", "activityName"})
@ToString(exclude = {"id", "activityName", "questions"})
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "uv_activity")
public class ValidationUnitActivity extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, name = "activity_name")
    private String activityName;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "uv", referencedColumnName = "id")
    private ValidationUnit validationUnit;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "uv_activity", referencedColumnName = "id")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Question> questions;

    public ValidationUnitActivity(Long id, String activityName, ValidationUnit validationUnit) {
        this.id = id;
        this.activityName = activityName;
        this.validationUnit = validationUnit;
    }
}
