package com.quest.etna.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static java.lang.Double.NaN;

@EqualsAndHashCode(of = {"userId", "promoId"})
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "user_promo_note")
@IdClass(UserPromoNoteId.class)
public class UserPromoNote {
    @Id
    private Long userId;

    @Id
    private Long promoId;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "user")
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "promotion")
    private Promotion promotion;

    @Column(nullable = false)
    private Double note;

    public UserPromoNote(User foundUser, Promotion promotion, Double midNote) {
        this.user = foundUser;
        this.userId = foundUser.getId();
        this.promotion = promotion;
        this.promoId = promotion.getId();
        this.note = !midNote.isNaN()?midNote:0.0;
    }

    public UserPromoNoteId getId() {
        return new UserPromoNoteId(userId, promoId);
    }

}
