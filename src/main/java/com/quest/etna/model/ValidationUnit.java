package com.quest.etna.model;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@ToString(exclude = {"validationUnitActivityList"})
@EqualsAndHashCode(callSuper = false, exclude = {"validationUnitActivityList"})
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationUnit extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(name = "long_name")
    private String longName;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "uv", referencedColumnName = "id")
    private List<ValidationUnitActivity> validationUnitActivityList;
}
