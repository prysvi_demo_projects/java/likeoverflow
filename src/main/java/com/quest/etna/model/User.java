package com.quest.etna.model;


import com.quest.etna.annotation.ReflectionUpdate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


//@ToString(exclude = {"technologies", "questions", "timelines", "comments", "messages", "chatMessages", "userMessageRatings"})
@ToString(of = {"id","username","etnaLogin"})
@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@Entity
public class User extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    private String username;

    @Column(name = "etna_login", nullable = false, unique = true)
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    private String etnaLogin;

    @Column(nullable = false)
    private String password;

    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @Column
    private Integer reputation = 80;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserRole role = UserRole.ROLE_USER;


    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "promotion", referencedColumnName = "id")
    private Promotion promotion;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "grade", referencedColumnName = "id")
    private Grade grade;

    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(
            name = "user_technology",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "technology_id"))
    private List<Technology> technologies;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<Timeline> timelines;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<Comment> comments;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<Message> messages;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<ChatMessage> chatMessages;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<Question> questions;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<UserMessageRating> userMessageRatings;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "user", referencedColumnName = "id")
    private List<UserPromoNote> userPromoNotes;

    @Column(name = "img_url")
    private String imgUrl;

    @Column(columnDefinition = "tinyint(1) default 1")
    private boolean enabled;

    //Called by tests
    public User(Long id, String username, String password, UserRole role) {
        this.id = id;
        this.username = username;
        this.etnaLogin = username;
        this.password = password;
        this.role = role;
    }

    public User(String username, String etnaLogin, UserRole role, String imgUrl, boolean enabled, Promotion promotion) {
        this.username = username;
        this.etnaLogin = etnaLogin;
        this.password = UUID.randomUUID().toString();
        this.role = role;
        this.imgUrl = imgUrl;
        this.enabled = enabled;
        this.promotion = promotion;
    }

//    public User(EtnaUser etnaUser, Promotion promotion) {
//        this.username = etnaUser.getEmail() != null ? etnaUser.getEmail() : etnaUser.getLogin() + "@etna-alternance.net";
//        this.etnaLogin = etnaUser.getLogin() != null ? etnaUser.getLogin() : etnaUser.getEmail();
//        this.password = UUID.randomUUID().toString();
//        this.role = UserRole.ROLE_USER;
//        this.promotion = promotion;
//    }


//    public User(String username, String password, UserRole role, String imgUrl, Promotion promotion) {
//        this.username = username;
//        this.password = password;
//        this.role = role;
//        this.imgUrl = imgUrl;
//        this.promotion = promotion;
//    }
//
//    public String getEtnaLogin() {
//        if (!username.isEmpty() && username.contains("@")) {
//            return username.substring(0, username.indexOf("@"));
//        } else {
//            return username;
//        }
//    }
}