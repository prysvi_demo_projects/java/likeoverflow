package com.quest.etna.model.etna;

import lombok.Data;
import org.springframework.http.ResponseCookie;

import java.util.Date;

@Data
public class EtnaCookie {
    private Date expiredAt;
    private ResponseCookie cookie;

    public EtnaCookie(ResponseCookie cookie) {
        this.cookie = cookie;
        long millis = cookie.getMaxAge().getSeconds() > 0 ? System.currentTimeMillis() + cookie.getMaxAge().toMillis() : 0;
        expiredAt = new Date(millis);
    }
}
