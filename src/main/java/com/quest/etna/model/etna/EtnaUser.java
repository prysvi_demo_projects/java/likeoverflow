package com.quest.etna.model.etna;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EtnaUser {
    private int id;
    private String login;
    private String firstname;
    private String lastname;
    private String promo;
    private String email;
    private boolean close;
    private Date deleted_at;
}
