package com.quest.etna.model.etna;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class EtnaPromo {
    private long id;
    private String target_name;
    private String term_name;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date learning_start;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date learning_end;
    private int learning_duration;
    private String promo;
    private String spe;
    private String wall_name;
}
