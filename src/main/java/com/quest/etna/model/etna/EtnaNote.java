package com.quest.etna.model.etna;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EtnaNote {
    private String uv_name;
    private String uv_long_name;
    private String activity_name;
    private Double student_mark;
}
