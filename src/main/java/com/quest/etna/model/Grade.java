package com.quest.etna.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@ToString(exclude = {"users"})
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Grade extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(name = "img_url")
    private String imgUrl;


    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "grade", referencedColumnName = "id")
    private List<User> users;
}