package com.quest.etna.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.quest.etna.annotation.ReflectionUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
        property = "@id")
@MappedSuperclass
public abstract class BaseModel {
    public static Logger _log = LoggerFactory.getLogger(BaseModel.class);
    @ReflectionUpdate(action = ReflectionUpdate.Action.IGNORE)
    @Column(name = "creation_date")
    protected Date creationDate;

    @ReflectionUpdate(action = ReflectionUpdate.Action.RECREATE)
    @Column(name = "updated_date")
    protected Date updatedDate;

    @PreUpdate
    public void preUpdateFunction() {
        this.updatedDate = new Date();
    }

    @PrePersist
    public void prePersistFunction() {
        this.creationDate = new Date();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
