package com.quest.etna.model;

public enum Reaction {
    UP(1), DOWN(-1);

    private final Integer rate;

    Reaction(final Integer rate) {
        this.rate = rate;
    }

    public Integer getRate() {
        return rate;
    }
}
