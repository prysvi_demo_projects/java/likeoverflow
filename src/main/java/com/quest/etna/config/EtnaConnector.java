package com.quest.etna.config;

import com.quest.etna.exception.EtnaApiException;
import com.quest.etna.model.etna.EtnaCookie;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.Date;

@RequiredArgsConstructor
@Slf4j
@Data
@Component
public class EtnaConnector {

    private final AppProperties appProperties;

    private final String cookieName = "authenticator";


    private WebClient webClient;
    private EtnaCookie etnaCookie;


    public WebClient getConnectedWebClient() {
        if (webClient == null) {
            initClient();
        }
        return webClient;
    }

    private void initClient() {
        authenticate();
        webClient = WebClient.builder()
                .filter((request, next) -> {
                    return next.exchange(ClientRequest.from(request)
                            .cookie(cookieName, getCookieToken()).build());
                })
                .filter(on401error())
                .build();
        authenticate();
    }

    public String getCookieToken() {
        if (null == etnaCookie || null == etnaCookie.getExpiredAt() || etnaCookie.getExpiredAt().before(new Date())) {
            authenticate();
        }
        return etnaCookie.getCookie().getValue();
    }


    private void authenticate() {
        // CONTENT REMOVED FOR SECURITY REASONS =)
    }


    private ExchangeFilterFunction on401error() {
        return (request, next) -> next.exchange(request).flatMap(response -> {
            if (response.statusCode().is4xxClientError()) {
                log.debug("Reload token");
                return Mono.just(getCookieToken()) //here you get a Mono with a new & valid token
                        .map(token -> ClientRequest
                                .from(request)
                                .cookie(cookieName, getCookieToken()).build())
                        .flatMap(next::exchange);
            }
            return Mono.just(response);
        });
    }


}
