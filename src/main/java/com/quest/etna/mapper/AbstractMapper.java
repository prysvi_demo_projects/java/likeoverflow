package com.quest.etna.mapper;



import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMapper<D, O> {
    public final IMapper<D, O> mapper;

    protected AbstractMapper(IMapper<D, O> mapper) {
        this.mapper = mapper;
    }

    public ArrayList<D> objListToDtoList(List<O> objList) {
        ArrayList<D> list = new ArrayList<>();
        objList.forEach(account -> list.add(objToDto(account)));
        return list;
    }

    public D objToDto(O tObj) {
        if (tObj == null)
            return null;
        else
            return mapper.toDto(tObj);
    }

    public O objFromDto(D tDto) {
        if (tDto == null)
            return null;
        else
            return mapper.fromDto(tDto);
    }

    interface IMapper<D, O> {
        D toDto(O o);
        O fromDto(D o);
    }


}
