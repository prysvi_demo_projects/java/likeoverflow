package com.quest.etna.mapper;

import com.quest.etna.dto.AddressDTO;
import com.quest.etna.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class AddressMapper extends AbstractMapper<AddressDTO, Address> {
    public AddressMapper() {
        super(Mappers.getMapper(IAddressMapper.class));
    }

    @Mapper
    interface IAddressMapper extends AbstractMapper.IMapper<AddressDTO, Address> {
    }
}