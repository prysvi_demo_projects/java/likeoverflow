package com.quest.etna.mapper;

import com.quest.etna.dto.QuestionDTO;
import com.quest.etna.model.Question;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class QuestionMapper extends AbstractMapper<QuestionDTO, Question> {
    public QuestionMapper() {
        super(Mappers.getMapper(IQuestionMapper.class));
    }

    @Mapper
    interface IQuestionMapper extends IMapper<QuestionDTO, Question> {
    }
}