package com.quest.etna.mapper;

import com.quest.etna.dto.UserMessageRatingDTO;
import com.quest.etna.model.UserMessageRating;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class UserMessageRatingMapper extends AbstractMapper<UserMessageRatingDTO, UserMessageRating> {
    public UserMessageRatingMapper() {
        super(Mappers.getMapper(IUserMessageRatingMapper.class));
    }

    @Mapper
    interface IUserMessageRatingMapper extends IMapper<UserMessageRatingDTO, UserMessageRating> {
    }
}