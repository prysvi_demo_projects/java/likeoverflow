package com.quest.etna.mapper;

import com.quest.etna.dto.MessageDTO;
import com.quest.etna.model.Message;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class MessageMapper extends AbstractMapper<MessageDTO, Message> {
    public MessageMapper() {
        super(Mappers.getMapper(IMessageMapper.class));
    }

    @Mapper
    interface IMessageMapper extends IMapper<MessageDTO, Message> {
    }
}