package com.quest.etna.mapper;

import com.quest.etna.dto.ValidationUnitDTO;
import com.quest.etna.model.ValidationUnit;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class ValidationUnitMapper extends AbstractMapper<ValidationUnitDTO, ValidationUnit> {
    public ValidationUnitMapper() {
        super(Mappers.getMapper(IValidationUnitMapper.class));
    }

    @Mapper
    interface IValidationUnitMapper extends IMapper<ValidationUnitDTO, ValidationUnit> {
    }
}