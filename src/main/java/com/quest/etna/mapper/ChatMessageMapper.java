package com.quest.etna.mapper;

import com.quest.etna.dto.ChatMessageDTO;
import com.quest.etna.model.ChatMessage;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class ChatMessageMapper extends AbstractMapper<ChatMessageDTO, ChatMessage> {
    public ChatMessageMapper() {
        super(Mappers.getMapper(IChatMessageMapper.class));
    }

    @Mapper
    interface IChatMessageMapper extends IMapper<ChatMessageDTO, ChatMessage> {
    }
}