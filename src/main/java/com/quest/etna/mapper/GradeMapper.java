package com.quest.etna.mapper;

import com.quest.etna.dto.GradeDTO;
import com.quest.etna.model.Grade;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class GradeMapper extends AbstractMapper<GradeDTO, Grade> {
    public GradeMapper() {
        super(Mappers.getMapper(IGradeMapper.class));
    }

    @Mapper
    interface IGradeMapper extends IMapper<GradeDTO, Grade> {
    }
}