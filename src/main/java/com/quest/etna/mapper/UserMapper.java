package com.quest.etna.mapper;

import com.quest.etna.dto.UserDTO;
import com.quest.etna.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class UserMapper extends AbstractMapper<UserDTO, User> {
    public UserMapper() {
        super(Mappers.getMapper(IUserMapper.class));
    }

    @Mapper
    interface IUserMapper extends IMapper<UserDTO, User> {
    }
}