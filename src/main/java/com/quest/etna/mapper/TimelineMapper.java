package com.quest.etna.mapper;

import com.quest.etna.dto.TimelineDTO;
import com.quest.etna.model.Timeline;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

public class TimelineMapper extends AbstractMapper<TimelineDTO, Timeline> {
    public TimelineMapper() {
        super(Mappers.getMapper(ITimelineMapper.class));
    }

    @Mapper
    interface ITimelineMapper extends IMapper<TimelineDTO, Timeline> {
    }
}